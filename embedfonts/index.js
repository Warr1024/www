'use strict';

const fontlist = ['Noto Sans', 'Oxygen Mono'];

process.on('unhandledRejection', e => { throw e; });

const assert = require('assert');
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const util = require('util');
const needle = require('needle');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const md5 = data => crypto.createHash('md5')
	.update(data)
	.digest('hex');

const needleopts = {
	user_agent: 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0',
	follow: 20,
	decode_response: false,
	parse_response: false,
	compressed: true
};

async function webget(url, attempt) {
	attempt = attempt || 0;

	const cachefile = path.join(__dirname, 'cache', `${md5(url)}.raw`);
	try {
		return await readFile(cachefile);
	} catch (err) {}

	const resp = await needle('get', url, undefined, needleopts);
	console.warn(`${url}: ${resp.statusCode}`);

	if(attempt < 5 && (resp.statusCode === 503 || resp.statusCode === 429)) {
		await new Promise(r => setTimeout(r, Math.random() * 1000));
		return await webget(url, attempt + 1);
	}
	assert.strictEqual(resp.statusCode, 200);

	const body = Buffer.isBuffer(resp.body) ? resp.body : Buffer.from(resp.body);
	await writeFile(cachefile, body);

	return body;
}

async function getfont(name) {
	const css = await webget(
		`https://fonts.googleapis.com/css?family=${encodeURIComponent(name)}`);
	const parts = Object.assign(...css.toString()
		.split(/\/\*\s*/)
		.map(s => s.split(/\s*\*\//, 2))
		.map(([k, v]) => ({
			[k]: v
		})));
	const section = parts.latin
		.trim()
		.replace(/\s+/g, ' ');
	const woffurl = section.replace(/.*url\(/, '')
		.replace(/\).*/, '');
	const woff = await webget(woffurl);
	return section.replace(/url\([^\)]*\)/, `url(data:font/woff2;base64,${
		woff.toString('base64')})`);
}

async function main() {
	const [html, ...fonts] = await Promise.all([
		util.promisify(fs.readFile)(0),
		...fontlist.map(async x => await getfont(x))
	]);
	process.stdout.end(html.toString()
		.replace('</head>',
			`<style type="text/css">${fonts.join(' ')}</style></head>`));
}

main();
