'use strict';

process.on('unhandledRejection', e => { throw e; });

const assert = require('assert');
const fs = require('fs');
const util = require('util');
const path = require('path');
const crypto = require('crypto');
const needle = require('needle');
const cheerio = require('cheerio');
const jimp = require('jimp');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const md5 = data => crypto.createHash('md5').update(data).digest('hex');

const needleopts = {
	follow: 20,
	decode_response: false,
	parse_response: false,
	compressed: true
};

async function webget(url, attempt) {
	attempt = attempt || 0;

	const opts = Object.assign({ headers: {} }, needleopts);

	const cachefile = path.join(__dirname, 'cache', `${md5(url)}.json`);
	const cached = await (async () => {
		try {
			return JSON.parse(await readFile(cachefile));
		} catch (err) { }
	})();
	if (cached && cached.modtime)
		opts.headers['If-Modified-Since'] = cached.modtime;
	if (cached && cached.etag)
		opts.headers['If-None-Match'] = cached.etag;

	const resp = await needle('get', url, {}, opts);
	console.warn(`${url}: ${resp.statusCode}`);

	if (cached && resp.statusCode === 304)
		return Buffer.from(cached.body, 'base64');
	if (attempt < 5 && (resp.statusCode === 503 || resp.statusCode === 429)) {
		await new Promise(r => setTimeout(r, Math.random() * 1000));
		return await webget(url, attempt + 1);
	}
	assert.strictEqual(resp.statusCode, 200);

	const body = Buffer.isBuffer(resp.body) ? resp.body : Buffer.from(resp.body);
	await writeFile(cachefile, JSON.stringify({
		modtime: resp.headers['last-modified'],
		etag: resp.headers.etag,
		body: body.toString('base64')
	}));

	return body;
}

function handlerel(rel, base) {
	if (!rel) return;
	return new URL(rel, new URL(base))
		.toString();
}

const targetsize = 64;
const marginsize = targetsize + 4;
const bgcolor = 0xeeeeeeff;
const avatars = [];
async function handleavatar(rel, base) {
	const url = handlerel(rel, base);
	if (!url) return;
	try {
		const yoink = await jimp.read(await webget(url));
		const maxdim = Math.max(yoink.getWidth(), yoink.getHeight());
		yoink.background(bgcolor);
		yoink.contain(maxdim, maxdim);

		const img = await new jimp(maxdim, maxdim, bgcolor);
		img.composite(yoink, 0, 0);

		let upscale = maxdim;
		if (maxdim < targetsize) {
			upscale = maxdim * Math.ceil(targetsize / maxdim);
			img.resize(upscale, upscale, jimp.RESIZE_NEAREST_NEIGHBOR);
		}

		const padsize = Math.ceil(marginsize / targetsize * upscale / 2) * 2;
		const pad = await new jimp(padsize, padsize, bgcolor);
		const padoffs = (padsize - upscale) / 2
		pad.composite(img, padoffs, padoffs);

		pad.resize(marginsize, marginsize, jimp.RESIZE_BICUBIC);
		const cropoffs = (marginsize - targetsize) / 2;
		pad.crop(cropoffs - 1, cropoffs - 1, targetsize + 2, targetsize + 2);

		const text = (await pad.getBase64Async('image/png'))
			.replace(/^[^,]*base64,/, '');
		const hash = crypto.createHash('md5').update(text).digest('hex');
		const title = `$:/GitLab/Avatar/${hash}`;
		avatars.push({
			title,
			text,
			type: 'image/png',
			tags: 'GitLabSync GitLabAvatar'
		});
		return title;
	}
	catch (err) {
		console.warn(`${url}: ${err.message}`);
	}
}

async function projectdate(href) {
	const url = `${href.trimEnd('/')}/-/branches/all`;

	const body = (await webget(url)).toString();
	const $ = cheerio.load(`<div>${body}</div>`);

	let maxtime;
	$('li.branch-item time.js-timeago')
		.each((idx, el) => {
			const dt = $(el).attr('datetime');
			if (!maxtime || dt > maxtime)
				maxtime = dt;
		});

	return maxtime;
}

async function scrape(uripart, entsel, namesel, linksel, descsel, gettime) {
	const url = `https://gitlab.com/users/Warr1024/${uripart}.json`;
	try {
		const body = JSON.parse((await webget(url)).toString());
		assert.ok(body.html);
		const $ = cheerio.load(`<div>${body.html}</div>`);

		const list = [];
		const tasks = [];
		$(entsel)
			.each(async (idx, el) => tasks.push((async () => {
				if ($('span.badge-warning', el)
					.text())
					return;
				const name = $(namesel, el)
					.text()
					.trim();
				const href = handlerel($(linksel, el)
					.attr('href'), url);
				const avatar = await handleavatar($('img.gl-avatar', el)
					.attr('src'), url);
				const time = gettime && await projectdate(href);
				const desc = $(descsel, el)
					.text()
					.trim()
					.replace(/\n.*/, '');
				const slug = href.replace(/.*\//, '');
				list.push({ name, href, slug, desc, avatar, time });
			})()));
		await Promise.all(tasks);
		return list;
	} catch (err) {
		err.message = `${url}: ${err.message}`;
		throw err;
	}
}

async function scrapestandalone() {
	return await scrape('projects',
		'li.project-row',
		'span.project-name',
		'a[href]',
		'.description',
		true);
}

async function scrapegrouped() {
	const groups = await scrape('groups',
		'ul.content-list li',
		'a.group-name',
		'a.group-name',
		'.description');
	return (await Promise.all(groups.map(async g => {
		const url = `https://gitlab.com/groups/${g.slug}/-/children.json`;
		const body = JSON.parse((await webget(url)).toString());
		assert(Array.isArray(body));
		return await Promise.all(body.map(async p => Object.assign({
			name: p.name,
			href: handlerel(p.relative_path, url),
			avatar: await handleavatar(p.avatar_url, url),
			time: await projectdate(handlerel(p.relative_path, url)),
			desc: p.description,
		}, ...Object.entries(g)
			.map(([k, v]) => ({
				[`group${k}`]: v
			}))
		)));
	})))
		.flat();
}

async function main() {
	const projects = (await Promise.all([
		scrapestandalone(),
		scrapegrouped()
	]))
		.flat();
	const tiddlers = projects.map(x => ({
		title: `GitLab/${x.groupname || 'Warr1024'}/${x.name}`,
		caption: x.groupname ? `${x.groupname} / ${x.name}` : x.name,
		text: '',
		description: x.desc,
		name: x.name,
		url: x.href,
		avatar: x.avatar,
		listorder: x.time && x.time.replace(/[^0-9]/g, ''),
		time: x.time && x.time.replace('T', ' ')
			.replace(/:\d\d\.\d{3}Z$/, ''),
		group_name: x.groupname,
		group_url: x.grouphref,
		group_avatar: x.groupavatar,
		group_desc: x.groupdesc,
		tags: 'GitLabSync GitLabProject'
	}));
	tiddlers.push(...avatars);
	for (let t of tiddlers)
		Object.keys(t)
			.filter(k => !t[k] && t[k] !== '')
			.forEach(k => { delete t[k]; });
	process.stdout.write(JSON.stringify(tiddlers, null, '\t'));
	process.stderr.end('\n');
}

main();
