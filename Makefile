tw=node ./node_modules/.bin/tiddlywiki

build:
	rm -rf dist
	mkdir -p dist
	make build-index build-archives

build-index: node_modules embedfonts/node_modules rescrape
	mkdir -p tmp
	rm -rf tmp/wiki
	cp -R wiki tmp/wiki
	${tw} tmp/wiki --load publish.json
	${tw} tmp/wiki --output `pwd`/tmp \
		--render '[[$$:/core/save/all]]' index.html text/plain
	node embedfonts <tmp/index.html >dist/index.html

build-archives:
	cd archive && ls | xargs -rtn 1 -P 4 -I _ 7z a -bd -mmt=on \
		-tzip -mx=9 -mfb=258 -mpass=15 ../dist/_.zip _

edit: node_modules rescrape
	`which xdg-open open | head -n 1` http://127.0.0.1:34080/ ||:
	exec ${tw} wiki --listen \
		host=127.0.0.1 \
		port=34080 \
		anon-username=Warr1024

rescrape: gitlabscrape/node_modules
	${tw} wiki --deletetiddlers '[tag[GitLabSync]]'
	mkdir -p tmp
	node gitlabscrape >tmp/import.json
	${tw} wiki --import tmp/import.json application/json

gitlabscrape/node_modules:
	cd gitlabscrape && make node_modules

embedfonts/node_modules:
	cd embedfonts && make node_modules

node_modules:
	npm ci -ddd
