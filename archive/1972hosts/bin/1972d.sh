#!/bin/sh

basedir=/var/1972hosts

read cmd
if [ "$?" != "0" ]; then
	exit 0
fi

if [ "x$cmd" = "xpush" ]; then
	$basedir/bin/1972pull.sh &
	exit 0
fi

if [ "x$cmd" != "x`cat $basedir/var/hostlist.gz.md5`" ]; then
	cat $basedir/var/hostlist.gz
	exit 0
fi
