#!/bin/sh

basedir=/var/1972hosts
. $basedir/etc/1972hosts.conf

if [ "x${pull_host}" = "x" ]; then
	exit 0
fi

tmpd=/tmp/${0##*/}-`whoami`-$$
mkdir -p $tmpd
chmod 700 $tmpd

cat $basedir/var/hostlist.gz.md5 | nc ${pull_host} 21667 >$tmpd/conf-gz
if [ "$?" != "0" ]; then
	rm -Rf $tmpd
        exit 1
fi

if [ "x`gzcat $tmpd/conf-gz 2>/dev/null`" != "x" ]; then
	chown 0:0 $tmpd/conf-gz
	chmod 644 $tmpd/conf-gz
	mv $tmpd/conf-gz $basedir/var/hostlist.gz
	. /var/1972hosts/bin/1972update.sh
fi

rm -Rf $tmpd
