#!/bin/sh

if [ "x$1" != "x" ]; then
	echo 'push' | nc $1 21667
	exit 0
fi

basedir=/var/1972hosts
. $basedir/etc/1972hosts.conf
tmpd=/tmp/${0##*/}-`whoami`-$$
mkdir -p $tmpd
chmod 700 $tmpd

mycmd=$0
set -- ${push_hosts}
while [ "x$1" != "x" ]; do
	echo -n 'Notifying '$1'...'
	$mycmd $1 >/dev/null 2>&1 &
	echo ' done.'
	shift 1
done

rm -Rf $tmpd
