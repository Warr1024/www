#!/bin/sh

stripcom() {
	local _file="$1"
	local _line
	while read _line ; do
		_line=${_line%%#*}
		test -z "$_line" && continue
		echo $_line
	done <$_file
}



#####	LOAD SETTINGS	#####

echo -n 'Loading settings...'
basedir=/var/1972hosts
. $basedir/etc/1972hosts.conf
tmpd=/tmp/${0##*/}-`whoami`-$$
mkdir -p $tmpd
chmod 700 $tmpd
gzcat $basedir/var/hostlist.gz >$tmpd/conf-raw
stripcom $tmpd/conf-raw >$tmpd/conf-strip
while read host ipad macad xtra; do
        if [ "x$host" = "x" ]; then
                continue
        fi
        if [ "x$ipad" = "x" ]; then
                continue
        fi
        echo $host $ipad $macad
done <$tmpd/conf-strip >$tmpd/conf
echo ' done.'

echo -n 'Updating address database hash...'
oldmd=`cat $basedir/var/hostlist.gz.md5`
gzcat $basedir/var/hostlist.gz | md5 >$basedir/var/hostlist.gz.md5
newmd=`cat $basedir/var/hostlist.gz.md5`
logger -t 1972update 'Host database updated; '$oldmd' -> '$newmd
echo ' done.'


#####         /ETC/HOSTS        #####
echo -n 'Creating new hosts list...'
while read host ipad macad; do
	echo $ipad' '${host}${this_lan}' '$host
done <$tmpd/conf >/etc/hosts
echo ' done.'



#####	DHCP SERVER SETTINGS	#####

echo -n 'Creating new dhcpd.conf...'
while read cmd param; do
	if [ "x$cmd" = "xhostlist" ]; then
		while read host ipad macad; do
			if [ "x${ipad#$param}" = "x$ipad" ]; then
				continue
			fi
			if [ "x$macad" = "x" ]; then
				continue
			fi
			echo 'host '$host' { hardware ethernet '$macad'; fixed-address '$ipad'; }'
		done <$tmpd/conf
		unset cmd
		unset param
	fi
	if [ "x$cmd" != "x" ]; then
		echo $cmd $param
	fi
done < $basedir/etc/dhcpd.conf.template >$tmpd/dhcpd.conf
chown 0:0 $tmpd/dhcpd.conf
chmod 644 $tmpd/dhcpd.conf
mv $tmpd/dhcpd.conf ${dhcpd_conf}
echo ' done.'

echo -n 'Restarting dhcpd...'
kill `cat ${dhcpd_pid}` >/dev/null 2>&1
touch /var/db/dhcpd.leases
${dhcpd_cmd}
echo ' done.'


#####	DNS SERVER SETTINGS	#####

echo -n 'Creating new forward zone...'
while :; do
	read cmd param
	if [ "$?" != "0" ]; then
		break
	fi
	if [ "x$cmd" = "xhostlist" ]; then
		while :; do
			read host ipad macad
			if [ "$?" != "0" ]; then
				break
			fi
			if [ "x${ipad#$param}" = "x$ipad" ]; then
				continue
                        fi
			echo ${host}${this_lan}'. IN A '$ipad
		done <$tmpd/conf
		unset cmd
		unset param
		continue
	fi
	if [ "x$cmd" = "x%" ]; then
		echo '	'$param
		continue
	fi
	echo $cmd $param
done <$basedir/etc/zone.a.template >$tmpd/zone.a
chown 0:0 $tmpd/zone.a
chmod 644 $tmpd/zone.a
mv $tmpd/zone.a ${fwd_zone}
echo ' done.'

echo -n 'Creating new reverse zone...'
while :; do
	read cmd param
	if [ "$?" != "0" ]; then
		break
	fi
	if [ "x$cmd" = "xhostlist" ]; then
		while :; do
			read host ipad macad
			if [ "$?" != "0" ]; then
				break
			fi
			if [ "x${ipad#$param}" = "x$ipad" ]; then
				continue
                        fi
			oct3=${ipad#*\.}
			oct2=${oct3#*\.}
			oct3=${oct3%%\.*}
			oct1=${oct2#*\.}
			oct2=${oct2%%\.*}
			echo $oct1'.'$oct2'.'$oct3' PTR '${host}${this_lan}.
		done <$tmpd/conf
		unset cmd
		unset param
		continue
	fi
	if [ "x$cmd" = "x%" ]; then
		echo '	'$param
		continue
	fi
	echo $cmd $param
done <$basedir/etc/zone.ptr.template >$tmpd/zone.ptr
chown 0:0 $tmpd/zone.ptr
chmod 644 $tmpd/zone.ptr
mv $tmpd/zone.ptr ${rev_zone}
echo ' done.'

echo -n 'Restarting BIND...'
kill `cat ${named_pid}` >/dev/null 2>&1
sleep .25
${named_cmd}
echo ' done.'



#####	CLEANUP	#####
rm -Rf $tmpd
exit 0
