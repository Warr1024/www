// $Id: geometry.h,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#ifndef __geometry_h
#define __geometry_h

#include "types.h"

INT xformall(WORLD *);
// Transforms all vertices in world into image coordinates.
// Returns 0 on success, non-zero on failure.  World camera
// information is no longer defined after return, but segments
// are not touched.

#endif
