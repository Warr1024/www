// $Id: render.c,v 1.3 2004/02/08 18:14:14 warr Exp $

#include <string.h>
#include <stdio.h>
#include "render.h"
#include "endian.h"
#include "debug.h"

#define PCXHEAD	struct pcxheader
#define PCXTAIL struct pcxpalette

PCXHEAD
	{
	BYTE	fmtid;
	BYTE	ver;
	BYTE	rle;
	BYTE	bpp;
	WORD	coordleft;
	WORD	coordtop;
	WORD	coordright;
	WORD	coordbot;
	WORD	dpix;
	WORD	dpiy;
	BYTE	pal16[48];
	BYTE	res1;
	BYTE	planes;
	WORD	bpl;
	WORD	paltype;
	BYTE	res2[58];
	};

PCXTAIL
	{
	BYTE	tag;
	BYTE	pal256[768];
	};

inline FLOAT quikdot(FLOAT x1, FLOAT y1, FLOAT x2, FLOAT y2)
	{
	return((x1 * x2) + (y1 * y2));
	}

inline INT writepcxheader(FILE *outf)
	{
	PCXHEAD pcxh;
	INT count1, count2;

	// Initialize PCX image header.
	memset((void *)(&pcxh), 0, sizeof(pcxh));
	pcxh.fmtid = 10;
	pcxh.ver = 5;
	pcxh.bpp = 8;
	pcxh.coordright = autoendian(255);
	pcxh.coordbot = autoendian(255);
	pcxh.dpix = autoendian(96);
	pcxh.dpiy = autoendian(96);
	for(count1 = 0; count1 < 16; count1 ++)
		for(count2 = 0; count2 < 3; count2++)
			pcxh.pal16[(count1 * 3) + count2] = 15 - count1;
	pcxh.planes = 1;
	pcxh.bpl = autoendian(256);
	pcxh.paltype = autoendian(2);

	// Write the PCX header (palette, for some reason, goes at end).
	if(fwrite((void *)(&pcxh), sizeof(pcxh), 1, outf) != 1)
		return(oops("Error writing PCX header."));

	return(0);

	}

inline INT writepcxtail(FILE *outf)
	{

	INT count1, count2;
	PCXTAIL pcxt;

	// Create palette.
	pcxt.tag = 12;
	for(count1 = 0; count1 < 256; count1 ++)
		for(count2 = 0; count2 < 3; count2++)
			pcxt.pal256[(count1 * 3) + count2] = count1;

	// Write palette out.
	if(fwrite((void *)(&pcxt), sizeof(pcxt), 1, outf) != 1)
		return(oops("Error writing PCX palette."));

	return(0);

	}

FLOAT segdistsqr(FLOAT x1, FLOAT y1, FLOAT x2, FLOAT y2, FLOAT cx, FLOAT cy)
	{

	FLOAT ptmp;

	// Initialize everything relative to origin (x1, y1).
	x2 -= x1; y2 -= y1; cx -= x1; cy -= y1;

	// If the edge has a length of 0, just render it as a dot.
	if(quikdot(x2, y2, x2, y2) == 0)
		return(quikdot(cx, cy, cx, cy));

	// First, check if we're adjacent to the edge, or past the ends.  This
	// determines whether we'll compute distance by vector projection or
	// by the "end cap" method.
	ptmp = quikdot(x2, y2, cx, cy) / quikdot(x2, y2, x2, y2);
	if(ptmp < 0)
		{
		// "end cap" at start of segment.
		ptmp = quikdot(cx, cy, cx, cy);
		return(ptmp);
		}
	if(ptmp > 1)
		{
		// "end cap" at end of segment.
		ptmp = quikdot(cx - x2, cy - y2, cx - x2, cy - y2);
		return(ptmp);
		}

	// We're not at either end cap, so we'll compute distance from segment by crossing
	// it with <0,0,1> to find the vector *across* our edge, then measuring where
	// we are along it by scalar projection.  Dot product with <0,0,1> is a trivial
	// operation in where our vector simply ends up as <y, -x>.
	ptmp = quikdot(y2, -x2, cx, cy);
	ptmp *= ptmp;
	ptmp /= quikdot(x2, y2, x2, y2);
	return(ptmp);
	
	}

INT renderall(WORLD *world, FILE *outf)
	{

	INT y, x, edge;
	FLOAT distsqr, gray;
	BYTE pxl;

	// Write our PCX header.
	if(writepcxheader(outf) != 0)
		return(1);

	// Step through each pixel.
	for(y = 0; y < 256; y++)
		for(x = 0; x < 256; x++)
			{

			gray = 0;

			for(edge = 0; edge < world->numedges; edge++)
				{

				// Compute the distance squared from the line segment.
				distsqr = segdistsqr(world->vertices[world->edges[edge].start].x,
					world->vertices[world->edges[edge].start].y, world->vertices[world->edges[edge].end].x,
					world->vertices[world->edges[edge].end].y, x, y);

				// Adjust distance multiplier to change edge width.
				distsqr *= 2;

				// Light cast by edge proportional to inverse distance squared, clipped at 255.
				if(distsqr < 1)
					gray += 255;
				else
					gray += 255 / distsqr;

				// We can stop processing edges once we've already maxed out our graylevel.
				if(gray >= 255)
					break;

				}

			// Clip gray value, convert to byte.
			if(gray > 255) gray = 255;
			pxl = (unsigned char)(gray);

			// Write pixel gray level to file.
			if(fwrite((void *)(&pxl), sizeof(BYTE), 1, outf) != 1)
				return(oops("Error writing pixel data at <%d, %d>.", x, y));

			}

	// Finish up by writing image grayscale palette.
	if(writepcxtail(outf) != 0)
		return(1);

	// Success.
	return(0);

	}
