// $Id: main.c,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#include <stdio.h>
#include <string.h>
#include "input.h"
#include "geometry.h"
#include "render.h"
#include "types.h"
#include "debug.h"

int main(int argc, char **argv)
	{

	FILE *inf, *outf;
	WORLD world;

	// Check for correct command line usage.
	if(argc != 3)
		return(oops("Usage: %s <inputfile> <outputfile>", argv[0]));

	// Attempt to open input file.
	if(strcmp(argv[1], "-") == 0)
		inf = stdin;
	else
		inf = fopen(argv[1], "r");
	if(inf == NULL)
		return(oops("Error: unable to open %s for reading.", argv[1]));

	// Attempt to open output file.
	if(strcmp(argv[2], "-") == 0)
		outf = stdout;
	else
		outf = fopen(argv[2], "w");
	if(outf == NULL)
		return(oops("Error: unable to open %s for writing.", argv[2]));

	// Read input
	if(readworld(inf, &world) != 0)
		return(1);
	fclose(inf);

	// Transform 3D->2D
	if(xformall(&world) != 0)
		return(1);

	// Render image to bitmap file.
	if(renderall(&world, outf) != 0)
		return(1);
	fclose(outf);

	// Successful run.
	return(0);
	
	}
