// $Id: render.h,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#ifndef __render_h
#define __render_h

#include <stdio.h>
#include "types.h"

INT renderall(WORLD *, FILE *);
// This renders the world as a grayscale image with cool-looking
// glowing edges.  It writes the entire PCX file format (head to
// tail) to the output file.  File will be a 256x256 grayscale
// PCX image.

#endif
