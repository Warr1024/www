// $Id: endian.c,v 1.1 2004/02/08 18:14:14 warr Exp $

#include "endian.h"
#include "types.h"

WORD autoendian(WORD q)
	{

	// Test the endianness of this machine
	WORD tw = 0x1234;
	BYTE *tb = (BYTE *)(&tw);

	// If big endian, swap bytes.
	if((*tb) == 0x12)
		return(((q & 0xFF) << 8) | ((q & 0xFF00) >> 8));

	// Leave little endian words untouched.
	return(q);

	}
