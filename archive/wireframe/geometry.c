// $Id: geometry.c,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#include <stdlib.h>
#include <math.h>
#include "geometry.h"
#include "types.h"
#include "debug.h"

inline void vectorcross(VECTOR *a, VECTOR *b, VECTOR *prod)
	{
	prod->x = (a->y * b->z) - (a->z * b->y);
	prod->y = (a->z * b->x) - (a->x * b->z);
	prod->z = (a->x * b->y) - (a->y * b->x);
	}

inline FLOAT vectordot(VECTOR *a, VECTOR *b)
	{
	return((a->x * b->x) + (a->y * b->y) + (a->z * b->z));
	}

inline void normalize(VECTOR *a)
	{

	// Length of vector V is square root of V dot V.
	FLOAT l = sqrt(vectordot(a, a));

	// Scalar divide vector by its length.  This makes vector
	// point same direction, but now its magnitude is 1.
	a->x /= l;
	a->y /= l;
	a->z /= l;

	}

inline void vectorsub(VECTOR *a, VECTOR *b)
	{
	a->x -= b->x;
	a->y -= b->y;
	a->z -= b->z;
	}

inline void matrixmul(VECTOR *rx, VECTOR *ry, VECTOR *rz, VECTOR *m)
	{

	// Anti-alias copy of m (which will be overwritten).
	VECTOR t = *m;

	// A matrix multiplication is a series of dot-products of
	// row and column vectors.  This is a shortcut (less coding)
	// for our rotation matrix.
	m->x = vectordot(rx, &t);
	m->y = vectordot(ry, &t);
	m->z = vectordot(rz, &t);

	}

void normall(WORLD *world)
	{

	INT count;

	// Restretch image plane -1 to 1 to 0 - 256
	for(count = 0; count < world->numverts; count++)
		{
		world->vertices[count].x = 128 + 128 * world->vertices[count].x;
		world->vertices[count].y = 128 + 128 * world->vertices[count].y;
		}

	}

INT xformall(WORLD *world)
	{

	VECTOR camx, camy, camz, zpl;
	INT count;

	// First, translate everything into camera-relative world coords.
	vectorsub(&(world->camera.target), &(world->camera.origin));
	for(count = 0; count < world->numverts; count++)
		vectorsub(&(world->vertices[count]), &(world->camera.origin));

	// Next, normalize the camera's z axis unit vector.
	camz = world->camera.target;
	normalize(&camz);

	// The camera's x axis is always parallel to the world x-y plane, so must
	// therefore be perpendicular to the world x-y plane's normal.  Since it's
	// also perpendicular to the camera's z, a method of finding it becomes
	// obvious...
	zpl.x = 0;
	zpl.y = 0;
	zpl.z = 1;
	vectorcross(&camz, &zpl, &camx);

	// Finally, z cross x = y.
	vectorcross(&camz, &camx, &camy);

	// Now, derivation using the scalar projection along a vector will show
	// that the transform matrix to rotate world coordinates into the frame
	// of camera coordinates is a matrix with camx, camy, and camz each
	// respectively down columns 1, 2, and 3 of the matrix.  We'll take a
	// shortcut and use a non-homogeneous matrix and vector system, since
	// no translation is taking place (so there's no constant term in the
	// system of equations)
	for(count = 0; count < world->numverts; count++)
		matrixmul(&camx, &camy, &camz, &(world->vertices[count]));

	// Now all vertices are in camera coordinates, we need only "squash"
	// them onto the plane of the image.
	for(count = 0; count < world->numverts; count++)
		{
		world->vertices[count].x *= world->camera.focal / world->vertices[count].z;
		world->vertices[count].y *= world->camera.focal / world->vertices[count].z;
		}

	// Lastly, we now have a 2D image, we need only to stretch it into
	// a 256x256 image space.  We'll allow 8 pixels of padding on each
	// side, so we'll normalize the X and Y values into 2D space between
	// 8 and 247.
	normall(world);

	return(0);

	}
