// $Id: debug.c,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#include <stdio.h>
#include <stdarg.h>
#include "debug.h"

INT oops(char *fmt, ...)
	{

	// Print error message.
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");

	// Return non-zero (indicates error).
	return(1);

	}
