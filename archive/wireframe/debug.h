// $Id: debug.h,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#ifndef __debug_h
#define __debug_h

#include "types.h"

INT oops(char *, ...);
// This function outputs a short error message
// (formatted as printf) to stderr, and returns
// non-zero, so it can be used to simultaneously
// spit out an error msg and return non-zero
// as in return(oops(...));

#endif
