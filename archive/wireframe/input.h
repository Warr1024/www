// $Id: input.h,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#ifndef __input_h
#define __input_h

#include "types.h"

INT readworld(FILE *, WORLD *);
// readworld reads the input file from file descriptor arg1,
// and loads information into WORLD structure arg2, creating
// NULL-terminated linked lists.  Returns 0 on success, non-
// zero on error.  arg2 contents undefined on non-zero return.

#endif
