// $Id: types.h,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#ifndef __types_h
#define __types_h

#define FLOAT	double
#define INT	int
#define BYTE	unsigned char
#define WORD	unsigned short int

#define VECTOR	struct vector_type
#define CAMERA	struct camera_type
#define EDGE	struct edge_type
#define WORLD	struct world_type

VECTOR
	{
	FLOAT	x;
	FLOAT	y;
	FLOAT	z;
	};

CAMERA
	{
	VECTOR	origin;
	VECTOR	target;
	FLOAT	focal;
	};

EDGE
	{
	INT	start;
	INT	end;
	};

WORLD
	{
	CAMERA	camera;
	VECTOR	*vertices;
	EDGE	*edges;
	INT	numverts;
	INT	numedges;
	};

#endif
