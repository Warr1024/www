// $Id: endian.h,v 1.1 2004/02/08 18:14:14 warr Exp $

#ifndef __endian_h
#define __endian_h

#include "types.h"

WORD autoendian(WORD);
// Automatically detects and converts big-endian to small-endian
// for PCX format.

#endif
