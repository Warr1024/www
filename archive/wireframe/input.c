// $Id: input.c,v 1.1.1.1 2004/02/06 22:06:47 warr Exp $

#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#include "types.h"
#include "debug.h"

INT readworld(FILE *inf, WORLD *world)
	{

	INT count;

	// Read the 7 camera numbers.
	if(fscanf(inf, "%lf%lf%lf%lf%lf%lf%lf", &(world->camera.origin.x), &(world->camera.origin.y),
		&(world->camera.origin.z), &(world->camera.target.x), &(world->camera.target.y),
		&(world->camera.target.z), &(world->camera.focal)) != 7)
		return(oops("File structure error reading camera orientation."));

	// Find out how many vertices and edges to load.
	if(fscanf(inf, "%d%d", &(world->numverts), &(world->numedges)) != 2)
		return(oops("Input file structure error reading number of vertices and edges."));
	if(world->numverts < 2)
		return(oops("World must have at least two vertices."));
	if(world->numedges < 1)
		return(oops("World must have at least one edge."));

	// Load vertices directly into a dynamic array.
	world->vertices = (VECTOR *)(malloc(sizeof(VECTOR) * world->numverts));
	if(world->vertices == NULL)
		return(oops("Insufficient memory"));
	for(count = 0; count < world->numverts; count++)
		if(fscanf(inf, "%lf%lf%lf", &(world->vertices[count].x), &(world->vertices[count].y),
			&(world->vertices[count].z)) != 3)
			return(oops("Input file structure error reading vertex #%d.", count + 1));

	// Load edges directly into a dynamic array.
	world->edges = (EDGE *)(malloc(sizeof(EDGE) * world->numedges));
	if(world->edges == NULL)
		return(oops("Insufficient memory"));
	for(count = 0; count < world->numedges; count++)
		{
		if(fscanf(inf, "%d%d", &(world->edges[count].start), &(world->edges[count].end)) != 2)
			return(oops("File structure error reading edge #%d.", count + 1));
		world->edges[count].start--; world->edges[count].end--;
		if((world->edges[count].start < 0) || (world->edges[count].start >= world->numverts))
			return(oops("Edge #%d starts on invalid vertex #%d.", count, world->edges[count].start + 1));
		if((world->edges[count].end < 0) || (world->edges[count].end >= world->numverts))
			return(oops("Edge #%d ends on invalid vertex #%d.", count, world->edges[count].end + 1));
		}

	// Return success.
	return(0);

	}
