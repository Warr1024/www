// $Id: nonmax.c,v 1.4 2004/03/02 16:14:48 warr Exp $
//
// Non-Maximum Suppression Module
// IMPLEMENTATION

#include <stdlib.h>
#include <math.h>
#include "nonmax.h"
#include "debug.h"
#include "matrix.h"

// Suppresses all non-local-max values in matrix.
matrix *nonmax(matrix *mag, matrix *dir)
	{

	// Make sure we've got valid matrices to work with.
	trace("nonmax: check input");
	if((mag == NULL) || (dir == NULL))
		{
		oops("Inputs not valid to nomax");
		trace("nonmax: fail");
		return(NULL);
		}
	if((mag->width != dir->width) || (mag->height != dir->height))
		{
		oops("Matrix dimensions do not agree");
		trace("nonmax: fail");
		return(NULL);
		}

	// Create new output matrix
	int x = mag->width - 2;
	int y = mag->height - 2;
	trace("nonmax: create %dx%d matrix", x, y);
	matrix *rtn = matrix_create(x, y);
	if(rtn == NULL)
		{
		trace("nonmax: fail");
		return(NULL);
		}

	// Step through matrix.
	trace("nonmax: suppress operation over %dx%d matrix", mag->width, mag->height);
	for(x = 1; x < mag->width - 1; x++)
		for(y = 1; y < mag->height - 1; y++)
			{

			// Grab magnitude and direction cells.
			double *m = matrix_cell(mag, x, y);
			double *z = matrix_cell(rtn, x - 1, y - 1);
			double *d = matrix_cell(dir, x, y);

			// Verify cells are in bounds.
			if((m == NULL) || (d == NULL) || (z == NULL))
				return(NULL);

			// Find 2 neighbors.
			double *n1 = NULL, *n2 = NULL;
			double t = (*d);
			if(t > M_PI) t -= M_PI;
			if((t < M_PI / 8) || (t > M_PI * 7 / 8))
				{
				n1 = matrix_cell(mag, x + 1, y);
				n2 = matrix_cell(mag, x - 1, y);
				}
			if((t >= M_PI / 8) && (t < M_PI * 3 / 8))
				{
				n1 = matrix_cell(mag, x + 1, y + 1);
				n2 = matrix_cell(mag, x - 1, y - 1);
				}
			if((t >= M_PI * 3 / 8) && (t < M_PI * 5 / 8))
				{
				n1 = matrix_cell(mag, x, y + 1);
				n2 = matrix_cell(mag, x, y - 1);
				}
			if((t >= M_PI * 5 / 8) && (t < M_PI * 7 / 8));
				{
				n1 = matrix_cell(mag, x + 1, y - 1);
				n2 = matrix_cell(mag, x - 1, y + 1);
				}

			// Make sure we found neighbors.
			if(n1 == NULL)
				continue;
			if(n2 == NULL)
				continue;

			// Compare pixel to neighbors, suppress non-maximum values.
			if(((*m) < (*n1)) || ((*m) < (*n2)))
				(*z) = 0;
			else
				(*z) = (*m);

			}

	trace("nonmax: success");
	return(rtn);

	}
