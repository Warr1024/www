// $Id: corner.h,v 1.2 2004/03/02 16:14:48 warr Exp $
//
// Corner Detection Module
// INTERFACE

#ifndef __corner_h
#define __corner_h

#include "matrix.h"
// Need matrix datatype for prototyupes.
#include <stdio.h>
// Need FILE type for prototypes.

matrix *corners(matrix *, matrix *, double, double);
// This function computes the lambda matrix from the X
// and Y gradient matrices.  First double parameter
// is a sigma to find the mask size, and the second is
// a lambda threhold for corner rejection.  Returns
// a matrix of lambda values, or NULL on error.

int cornhole(matrix *, FILE *);
// Accepts matrix of lambda values (from corners() function)
// and a pointer to a FILE opened for writing.  Will create
// a sorted text report of all non-zero cells in lambda
// matrix.  Returns non-zero on error.

#endif
