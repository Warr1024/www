// $Id: matrix.h,v 1.2 2004/03/02 16:14:48 warr Exp $
//
// Matrix Library
// INTERFACE

#ifndef __images_h
#define __images_h

struct matrix
	{
	int width;
	int height;
	double *data;
	};
// Structure for matrix implemented in this library.

struct matrix *matrix_create(int, int);
// img_create creates a new image matrix with the given
// dimensions (width, height), and returns a pointer to
// the img_matrix_type structure.  Width and height must
// each be at least 1.  Returns NULL if error in dimensions
// or if not enough memory (malloc fails).

void matrix_delete(matrix *);
// This function cleanly deletes a properly initialized
// img_matrix_type object.

double *matrix_cell(matrix *, int, int);
// This function returs the pointer to the single byte
// for the given pixel (x, y) in the image.  Returns
// NULL if the pixel does not exist in the image (out
// of bounds).

#endif
