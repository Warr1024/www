// $Id: debug.c,v 1.4 2004/03/01 15:07:28 warr Exp $
//
// Debugging and Error Reporting Library
// IMPLEMENTATION

#include <stdarg.h>
#include <stdio.h>
#include "debug.h"

// Function for error reporting.
int oops(char *fmt, ...)
	{

	// Pull variable arguments into a list.
	va_list ap;
	va_start(ap, fmt);

	// Report the error message to stderr.
	fprintf(stderr, "ERROR: ");
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");

	// Flush output on stderr, in case we crash soon.
	fflush(stderr);

	// Return the error constant, so functions can
	// call oops() for their own return.
	return(ERROR);

	}

// Function for debugging output
void trace(char *fmt, ...)
	{

	// If compiled without -DDBGTRACE, this
	// function is just a stub.
#ifdef DBGTRACE

	// Pull variable arguments into a list.
	va_list ap;
	va_start(ap, fmt);

	// Output trace statement.
	fprintf(stderr, "TRACE: ");
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");

	// Flush stderr, in case we crash soon.
	fflush(stderr);

#endif

	}
