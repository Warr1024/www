// $Id: rectpol.h,v 1.3 2004/03/02 16:14:48 warr Exp $
//
// Rectangular->Polar Module
// INTERFACE

#ifndef __rectpol_h
#define __rectpol_h

#include "matrix.h"
// Need matrix datatype for function prototypes.

int rectpol(matrix *, matrix *, matrix *, matrix *);
// Accepts first matrix as x coordinates, second as y.
// Saves magnitude in third matrix, direction (in radians)
// in fourth.  Returns 0 on success else nonzero on error.

#endif
