// $Id: image.c,v 1.10 2004/03/02 16:14:48 warr Exp $
//
// PNG Image<->Matrix Library
// IMPLEMENTATION

#include <stdio.h>
#include <stdlib.h>
#include <libpng/png.h>
#include "image.h"
#include "matrix.h"
#include "debug.h"

// Read an image from a PNG file, already opened.
struct matrix *image_read(FILE *fp)
	{

	// Create structures to hold PNG info.
	trace("image_read: create png_ptr");
	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL, NULL, NULL);
	if(png_ptr == NULL)
		{
		oops("Out of memory");
		trace("image_read: fail");
		return(NULL);
		}
	trace("image_read: create info_ptr");
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if(info_ptr == NULL)
		{
		png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
		oops("Out of memory");
		trace("image_read: fail");
		return(NULL);
		}
	trace("image_read: create end_info");
	png_infop end_info = png_create_info_struct(png_ptr);
	if(end_info == NULL)
		{
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
		oops("Out of memory");
		trace("image_read: fail");
		return(NULL);
		}
	trace("image_read: setting PNG callback jump");
	if(setjmp(png_jmpbuf(png_ptr)) != 0)
		{
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		oops("Failed to set libpng callback jump");
		trace("image_read: fail");
		return(NULL);
		}

	// Initialize libPNG I/O for our file.
	trace("image_read: calling png_init_io");
	png_init_io(png_ptr, fp);

	// Read the image header.
	trace("image_read: read image headers");
	png_read_info(png_ptr, info_ptr);

	// Convert indexed images to RGB.
	trace("image_read: check for palette image");
	if (info_ptr->color_type == PNG_COLOR_TYPE_PALETTE)
		{
		trace("image_read: convert palette image to RGB");
		png_set_palette_to_rgb(png_ptr);
		}

	// Expand < 8-bit images to 8-bit.
	trace("image_read: check for < 8 bit depth");
	if((info_ptr->color_type == PNG_COLOR_TYPE_GRAY) && (info_ptr->bit_depth < 8))
		{
		trace("image_read: convert to 8-bit");
		png_set_gray_1_2_4_to_8(png_ptr);
		}

	// Strip 16-bit images to 8-bit
	trace("image_read: check for > 8 bit depth");
	if(info_ptr->bit_depth == 16)
		{
		trace("image_read: convert to 8-bit");
		png_set_strip_16(png_ptr);
		}

	// Strip off any alpha channel.
	trace("image_read: checking for alpha channel");
	if(info_ptr->color_type & PNG_COLOR_MASK_ALPHA)
		{
		trace("image_read: strip off alpha channel");
        	png_set_strip_alpha(png_ptr);
		}

	// Convert all color to grayscale.
	trace("image_read: check for color image");
	if ((info_ptr->color_type == PNG_COLOR_TYPE_RGB) || (info_ptr-> color_type == PNG_COLOR_TYPE_RGB_ALPHA))
		{
		trace("image_read: convert RGB to grayscale");
		png_set_rgb_to_gray_fixed(png_ptr, 1, -1, -1);
		}

	// Create space for image pixel data.
	int x = png_get_image_width(png_ptr, info_ptr);
	int y = png_get_image_height(png_ptr, info_ptr);
	trace("image_read: create image matrix, %dx%d", x, y);
	struct matrix *img = matrix_create(x, y);
	if(img == NULL)
		{
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
		trace("image_read: fail");
		return(NULL);
		}

	// Read in the rest of the PNG image.
	trace("image_read: allocate row_pointers");
	png_bytep *row_pointers = (png_bytep *)(malloc(y * sizeof(png_bytep)));
	if(row_pointers == NULL)
		{
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
		oops("Out of memory");
		trace("image_read: fail");
		return(NULL);
		}
	trace("image_read: allocate %d rows", y);
   	for(x = 0; x < y; x++)
		{

		// Create a row.
		row_pointers[x] = (png_bytep)(png_malloc(png_ptr, png_get_rowbytes(png_ptr, info_ptr)));

		// Verify we have enough memory.
		if(row_pointers[x] == NULL)
			{
			oops("Out of memory");
			trace("image_read: fail");
			return(NULL);
			}
		}

	trace("image_read: call png_read_image");
	png_read_image(png_ptr, row_pointers);

	// Copy PNG data into our image structure.
	trace("image_read: transfer pixels into %dx%d matrix", img->width, img->height);
	for(y = 0; y < img->height; y++)
		{

		// Get pointer to a whole row of 8-bit pixels.
		unsigned char *row = (unsigned char *)(row_pointers[y]);

		// Step through each pixel in row.
		for(x = 0; x < img->width; x++)
			{

			// Convert graylevel into 0-to-1 proportion intensity.
			double v = (double)(row[x]) / 255;

			// Find cell in matrix we're writing.
			double *a = matrix_cell(img, x, y);

			// If cell not found, try not to abort image.
			if(a == NULL)
				continue;

			// Assign matrix value.
			(*a) = v;

			}
		}

	// Clean up PNG image variables.
	trace("image_read: clean up png structures");
	png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
	for(y = 0; (unsigned int)(y) < png_get_image_height(png_ptr, info_ptr); y++)
		free(row_pointers[y]);
	free(row_pointers);

	trace("image_read: success");
	return(img);

	}

// Write image from matrix to PNG file.
int image_write(matrix *img, FILE *fp)
	{

	// Create structures for writing.
	trace("image_write: create png_ptr");
	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL, NULL, NULL);
	if(png_ptr == NULL)
		return(oops("Failed to allocate memory"));
	trace("image_write: create info_ptr");
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if(info_ptr == NULL)
		{
		png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
		return(oops("Failed to allocate memory"));
		}
	trace("image_write: set png jump vector");
	if (setjmp(png_jmpbuf(png_ptr)))
		{
		png_destroy_write_struct(&png_ptr, &info_ptr);
		return(oops("error setting PNG jump target"));
		}

	// Request maximum compression.
	trace("image_write: set maximum compression level");
	png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);

	// Prepare header for writing.
	trace("image_write: prepare image header data");
	png_set_IHDR(png_ptr, info_ptr, img->width, img->height,
		8, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT);

	// Start writing image header
	trace("image_write: png_init_io");
	png_init_io(png_ptr, fp);
	trace("image_write: png_write_info");
	png_write_info(png_ptr, info_ptr);

	// Read in the rest of the PNG image.
	int x, y;
	trace("image_write: allocate row_pointers");
	png_bytep *row_pointers = (png_bytep *)(malloc(img->height * sizeof(png_bytep)));
	if(row_pointers == NULL)
		{
		png_destroy_write_struct(&png_ptr, &info_ptr);
		return(oops("Out of memory"));
		}
	trace("image_read: allocate %d rows", img->height);
	for(x = 0; x < img->height; x++)
		{

		// Create a row.
		row_pointers[x] = (png_bytep)(png_malloc(png_ptr, png_get_rowbytes(png_ptr, info_ptr)));

		// Verify we have enough memory.
		if(row_pointers[x] == NULL)
			{
			png_destroy_write_struct(&png_ptr, &info_ptr);
			return(oops("Out of memory"));
			}
		}

	// Copy image data into PNG.
	trace("image_write: transfer each pixel from %dx%d matrix", img->width, img->height);
	for(y = 0; y < img->height; y++)
		{

		// Find pointer directly to row data.
		unsigned char *row = (unsigned char *)(row_pointers[y]);

		// Step through each pixel in row.
		for(x = 0; x < img->width; x++)
			{

			// Find pointer from matrix.
			double *a = matrix_cell(img, x, y);

			// If no value found at matrix, assign 0.
			if(a == NULL)
				{
				row[x] = 0;
				continue;
				}

			// Assign value from matrix to pixel.
			double q = *a;
			if(q < 0)
				q = 0;
			if(q > 1)
				q = 1;
			row[x] = (unsigned char)(q * 255);

			}
		}

	// Write the image data to file.
	trace("image_write: call png_write_image");
	png_write_image(png_ptr, row_pointers);
	trace("image_write: call png_write_end");
	png_write_end(png_ptr, info_ptr);

	// Clean up temporary data.
	trace("image_write: cleaning up");
	png_destroy_write_struct(&png_ptr, &info_ptr);
	for(y = 0; (unsigned int)(y) < png_get_image_height(png_ptr, info_ptr); y++)
		free(row_pointers[y]);
	free(row_pointers);

	trace("image_write: success");
	return(0);

	}
