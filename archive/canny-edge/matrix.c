// $Id: matrix.c,v 1.4 2004/03/02 16:14:48 warr Exp $
//
// Matrix Library
// IMPLEMENTATION

#include <stdlib.h>
#include "matrix.h"
#include "debug.h"

// Clear a new matrix.
struct matrix *matrix_create(int width, int height)
	{

	// Sanity check the width and height values.
	if(width < 1)
		{
		oops("Matrix width too small");
		return(NULL);
		}
	if(height < 1)
		{
		oops("Matrix height too small");
		return(NULL);
		}

	// create the outer structure, return on failure.
	struct matrix *mtx = (matrix *)(malloc(sizeof(matrix)));
	if(mtx == NULL)
		{
		oops("Out of memory");
		return(NULL);
		}

	// Set the width and height values.
	mtx->width = width;
	mtx->height = height;

	// Try to allocate the matrix memory block.
	mtx->data = (double *)(malloc(width * height * sizeof(double)));
	if(mtx->data == NULL)
		{
		free(mtx);
		oops("Not enough memory for %dx%d matrix.", width, height);
		return(NULL);
		}

	return(mtx);

	}

// Delete a matrix.
void matrix_delete(matrix *mtx)
	{

	// Delete the pixel data block.
	if(mtx->data != NULL)
		free(mtx->data);

	// Delete the rest.
	free(mtx);

	}

// Pointer to a particular pixel.
double *matrix_cell(matrix *mtx, int x, int y)
	{

	// Simple bounds checking.  Fail silently.
	if((x < 0) || (x >= mtx->width) || (y < 0) || (y >= mtx->height))
		return(NULL);

	// Compute memory location of matrix cell.
	return(&(mtx->data[y * mtx->width + x]));
	
	}
