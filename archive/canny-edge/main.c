// $Id: main.c,v 1.15 2004/03/02 22:53:28 triangle Exp $
//
// EE486 Project 2
//	Aaron Suen
//	Andrew Moyer
//	Ryan Smith
//
// MAIN MODULE

#include <stdlib.h>
#include <stdio.h>
#include "debug.h"
#include "matrix.h"
#include "image.h"
#include "gradient.h"
#include "rectpol.h"
#include "nonmax.h"
#include "threshold.h"
#include "corner.h"

// Display a command line usage error.
int usage(char *cmd)
	{

	// First, display standard error message.
	oops("Incorrect usage");

	// Display usage screen.
	fprintf(stderr, "Usage:\n\t%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
		cmd, " <input.png> <edges.png> <corners.txt>\n",
		"\t\t<sigma> <thi> <tlo> <tlambda>\n\n<input.png> is an input image. ",
		" All images are converted to grayscale.\n<edges.png> is the name of ",
		"a new .PNG image which will be created,\n\tcontaining the edges of t",
		"he original image outlined in white.\n<corners.txt> is the name of a",
		" new plaintext tab-delimited table, which\n\twill contain the X coor",
		"dinate, Y coordinate, and lambda value\n\tfor each corner found in t",
		"he image.\n\n<sigma> is a standard deviation to use in creation of t",
		"he gradient\n\tfilters.  The total mask will be at least 5 sigma on ",
		"each\n\taxis.  Note that the sigma mask size is also the mask size use",
		"d\n\tin image thinning and corner detection.\n<thi> is the high thre",
		"shold used for hysterisis thresholding.  Any\n\tpixel whose gradient",
		" magnitude is at least this value (after\n\tthinning) will be consid",
		"ered an edge.\n<tlo> is the low threshold for hysterisis thresholdin",
		"g.  Any pixel above\n\ttlo, which is connected to a pixel above thi b",
		"y other pixels at\n\tleast above tlo will be accepted as and edge.\n",
		"<tlambda> is the lambda value for the threshold of the corner detect",
		"ion\n\talgorithm.  Corners with lambda values lower than this will b",
		"e\n\trejected.\n\n");

	// Flush output.
	fflush(stderr);

	return(ERROR);

	}

// Main with arguments.
int main(int argc, char **argv)
	{

	// Check for correct command line usage.
	trace("main: argc=%d", argc);
	if(argc != 8)
		return(usage(argv[0]));

	// Read in and validate command line parameters.
	double sigma = strtod(argv[4], NULL);
	trace("main: sigma=%f", sigma);
	if(sigma <= 0)
		return(oops("Value for sigma must be greater than zero."));
	double thi = strtod(argv[5], NULL);
	trace("main: thi=%f", thi);
	double tlo = strtod(argv[6], NULL);
	trace("main: tlo=%f", tlo);
	double tlambda = strtod(argv[7], NULL);
	trace("main: tlambda=%f", tlambda);

	// Try to open command line files.
	trace("main: open input.png=%s", argv[1]);
	FILE *inpng = fopen(argv[1], "r");
	if(inpng == NULL)
		return(oops("Could not open %s for reading.", argv[1]));
	trace("main: open edges.png=%s", argv[2]);
	FILE *edgepng = fopen(argv[2], "w");
	if(edgepng == NULL)
		return(oops("Could not open %s for writing.", argv[2]));
	trace("main: open corners.txt=%s", argv[3]);
	FILE *cornertxt = fopen(argv[3], "w");
	if(cornertxt == NULL)
		return(oops("Could not open %s for writing.", argv[3]));

	// Read in the input image.
	trace("main: call image_read");
	struct matrix *initial = image_read(inpng);

	// Close input image.
	trace("main: close inpng");
	fclose(inpng);

	// Make sure read succeeded.
	trace("main: check initial");
	if(initial == NULL)
		return(ERROR);

	// Ensure input image is large enough when borders
	// get removed by later processing.
	int sz = (int)(5 * sigma + 1) + 5;
	trace("main: input image is %dx%d, min dimension is %d", initial->width, initial->height, sz);
	if((initial->width < sz) || (initial->height < sz))
		return(oops("Input image too small"));

	// Perform gaussian gradient filter on X.
	trace("main: calling gradient for x axis");
	matrix *grad1 = gradient(initial, sigma, false);
	if(grad1 == NULL)
		return(ERROR);

	// Perform gaussian gradient filter on Y.
	trace("main: calling gradient for Y axis");
	matrix *grad2 = gradient(initial, sigma, true);
	if(grad2 == NULL)
		return(ERROR);

	// Initial image is no longer needed.
	trace("main: deleting input image");
	matrix_delete(initial);

	// Convert gradients to polar coordinates.
	trace("main: calling rectpol");
	matrix *rho = matrix_create(grad1->width, grad1->height);
	matrix *theta = matrix_create(grad1->width, grad1->height);
	if(rectpol(grad1, grad2, rho, theta) != 0)
		return(ERROR);

	// Perform on-maximum suppression.
	trace("main: calling nonmax");
	matrix *max = nonmax(rho, theta);
	if(max == NULL)
		return(ERROR);
	trace("main: deleting rho, theta");
	matrix_delete(rho);
	matrix_delete(theta);

	// Perform hysterisis thresholding.
	trace("main: calling threshold");
	matrix *binary = threshold(max, thi, tlo);
	if(binary == NULL)
		return(ERROR);

	// Write out edge image.
	trace("main: writing image");
	if(image_write(binary, edgepng) != 0)
		return(ERROR);

	// Close edge.png file.
	trace("main: close edgepng");
	fclose(edgepng);

	// Compute corner matrix.
	trace("main: calling corners");
	matrix *corn = corners(grad1, grad2, sigma, tlambda);
	if(corn == NULL)
		return(ERROR);

	// Generate corner report.
	trace("main: calling cornhole");
	if(cornhole(corn, cornertxt) != 0)
		return(ERROR);

	// Close corner report file.
	trace("main: closing cornertxt");
	fclose(cornertxt);

	trace("main: success");
	return(0);

	}
