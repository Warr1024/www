// $Id: threshold.h,v 1.2 2004/03/02 19:28:40 warr Exp $
//
// Hysterisis Thresholding Module
// INTERFACE

#ifndef __threshold_h
#define __threshold_h

#include "matrix.h"
// Need matrix for function prototypes.

matrix *threshold(matrix *, double, double);
// Perform thresholding on matrix, using 2 thresholds (high and
// low).  Returns a matrix of 1's and 0's, or NULL on error.  This
// output matrix should be the final result image for Canny
// edge detection.

#endif
