// $Id: rectpol.c,v 1.3 2004/03/02 16:14:48 warr Exp $
//
// Rectangular->Polar Module
// IMPLEMENTATION

#include <stdlib.h>
#include <math.h>
#include "rectpol.h"
#include "matrix.h"
#include "debug.h"

// Convert rectangular coordinates to polar.
int rectpol(matrix *xr, matrix *yt, matrix *rh, matrix *th)
	{

	// Make sure matrices both initialized.
	trace("rectpol: input check");
	if((xr == NULL) || (yt == NULL) || (rh == NULL) || (th == NULL))
		return(oops("Bad matrix passed to rectpol"));
	if((xr->width != yt->width) || (xr->height != yt->height)
		|| (xr->width != rh->width) || (xr->height != rh->height)
		|| (xr->width != th->width) || (xr->height != th->height))
		return(oops("Matrix dimesions do not agree"));

	// Step through matrices converting coordinates.
	trace("rectpol: step through %dx%d matrices", yt->height, xr->width);
	int x, y;
	for(y = 0; y < yt->height; y++)
		for(x = 0; x < xr->width; x++)
			{

			// Get gradient components.
			double *xg = matrix_cell(xr, x, y);
			double *yg = matrix_cell(yt, x, y);
			double *rg = matrix_cell(rh, x, y);
			double *tg = matrix_cell(th, x, y);

			// Make sure we're not out of bounds.
			if((xg == NULL) || (yg == NULL) || (rg == NULL) || (tg == NULL))
				return(ERROR);

			// Compute magnitude of gradient.
			double rho = sqrt(((*xg) * (*xg)) + ((*yg) * (*yg)));

			// Compute raw arctan of gradient.
			double theta;
			if((*xg) == 0)
				{
				if((*yg) < 0)
					theta = -M_PI / 2;
				else
					theta = M_PI / 2;
				}
			else
				theta = atan((*yg) / (*xg));

			// Correct direction.
			if((*xg) < 0)
				theta += M_PI;
			if(theta < 0)
				theta += 2 * M_PI;

			// Store gradient polar data.
			(*rg) = rho;
			(*tg) = theta;

			}

	trace("rectpol: success");
	return(0);

	}
