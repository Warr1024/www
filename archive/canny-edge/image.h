// $Id: image.h,v 1.3 2004/03/01 15:27:00 warr Exp $
//
// PNG Image<->Matrix Library
// INTERFACE

#ifndef __matrix_h
#define __matrix_h

#include <stdio.h>
// Required for FILE * data type in prototypes.
#include "matrix.h"
// Required for matrix data type in prototypes.

struct matrix *image_read(FILE *fp);
// Reads a grayscale PNG image into a matrix, normalizing gray
// values between 0 and 1.  Returns a pointer to the input image
// matrix on success, NULL on failure.  File passed must already
// be open for reading.

int image_write(matrix *, FILE *);
// Writes matrix to a PNG file, using the same format that image_read
// returns.  0's are black, 1's are white.  Returns 0 on success,
// non-zero on any failure.

#endif
