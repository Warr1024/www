// $Id: corner.c,v 1.4 2004/03/02 16:14:48 warr Exp $
//
// Corner Detection Module
// IMPLEMENTATION

#include <stdlib.h>
#include <math.h>
#include "corner.h"
#include "matrix.h"
#include "debug.h"

// Produce a matrix of corners that meet lambda criterion.
matrix *corners(matrix *inpx, matrix *inpy, double sigma, double lambdath)
	{

	// Compute the mask size based on original sigma.
	int m = (int)(5 * sigma + 1) | 1;
	int bound = m >> 1;

	// Check that we were supplied valid input matrices
	trace("corners: check valid input");
	if((inpx == NULL) || (inpy == NULL))
		{
		oops("Invalid matrices supplied to corner detection");
		trace("corners: fail");
		return(NULL);
		}
	trace("corners: check dimensions match");
	if((inpx->width != inpy->width) || (inpx->height != inpy->height))
		{
		oops("Matrix dimension mismatch in corner detection");
		trace("corners: fail");
		return(NULL);
		}
	int x = inpx->width - bound * 2;
	int y = inpx->height - bound * 2;
	trace("corners: creating %dx%d matrix", x, y);
	matrix *rtn = matrix_create(x, y);
	if(rtn == NULL)
		{
		trace("corners: failed");
		return(NULL);
		}

	// Step through each pixel of the image.
	trace("corners: step through %dx%d image", inpx->width, inpx->height);
	for(y = bound; y < inpx->height - bound; y++)
		for(x = bound; x < inpx->width - bound; x++)
			{

			// Reset A, B, C coefficients
			double a = 0, b = 0, c = 0;

			// Locate output cell.  Continue on error.
			double *n = matrix_cell(rtn, x - bound, y - bound);
			if(n == NULL)
				continue;

			// Add up coefficients for entire neighborhood.
			for(int p = -bound; p < bound; p++)
				for(int q = -bound; q < bound; q++)
					{

					// Locate input cells.
					double *px = matrix_cell(inpx, x + p, y + q);
					double *py = matrix_cell(inpy, x + p, y + q);

					// On input cell error, try to continue.
					if(px == NULL)
						continue;
					if(py == NULL)
						continue;

					// Add to coefficients.
					a += ((*px) * (*px));
					b += ((*py) * (*py));
					c += ((*px) * (*py));

					}

			// Compute quadratic desciminant.
			double d = (a + b) * (a + b) - 4 * (a * b - c * c);

			// Compute lambda.  If complex, find absolute value.
			double l;
			if(d < 0)
				l = sqrt(((a + b) / 2) * ((a + b) / 2) + (d * d));
			else
				{
				l = (-a - b - sqrt(d)) / 2;
				l = sqrt(l * l);
				}

			// Store lambda if it meets the threshold.
			if(l >= lambdath)
				*n = l;
			else
				*n = 0;

			}

	x = rtn->width - bound * 2;
	y = rtn->height - bound * 2;
	trace("corners: create new %dx%d matrix", x, y);
	matrix *rtn2 = matrix_create(x, y);
	if(rtn2 == NULL)
		{
		trace("corners: failed");
		return(NULL);
		}

	// Suppress non-maximum values inside neighborhood.
	trace("corners: non-max suppression");
	for(y = bound; y < inpx->height - bound; y++)
		for(x = bound; x < inpx->width - bound; x++)
			{

			// Locate input and output cells, continue on error.
			double *px = matrix_cell(rtn, x, y);
			double *n = matrix_cell(rtn2, x - bound, y - bound);
			if((px == NULL) || (n == NULL))
				continue;

			// Copy input onto output cell.
			*n = *px;

			// Step through neighborhood.
			for(int p = -bound; p < bound; p++)
				for(int q = -bound; q < bound; q++)
					{

					// Locate neighbor cell, continue on error.
					double *py = matrix_cell(rtn, x + p, y + q);
					if(py == NULL)
						continue;

					// If neighbor is higher, suppress output cell.
					if(*px < *py)
						*n = 0;

					}

			}

	trace("corners: success");
	return(rtn2);

	}

// Private structure for constructing a binary search tree
// of corner coordinates.
struct BST
	{
	double x, y, l;
	BST *left, *right;
	};

// Insert node into binary search tree.
BST *insBST(BST *head, double x, double y, double l)
	{

	// If BST uninitialized, create head node.
	if(head == NULL)
		{
		head = (BST *)(malloc(sizeof(BST)));
		head->left = NULL;
		head->right = NULL;
		}

	// Start at head of tree.
	BST *tail = head;

	// Perform search of tree to find location
	// for new leaf node.
	while(true)
		{

		// Lambda goes to left of node.
		if(l < tail->l)
			{

			// Traverse if possible, else create new child.
			if(tail->left != NULL)
				tail = tail->left;
			else
				{
				tail->left = (BST *)(malloc(sizeof(BST)));
				tail = tail->left;
				break;
				}

			}

		// Lambda goes right of node.
		if(l >= tail->l)
			{

			// Traverse if possible.
			if(tail->right != NULL)
				tail = tail->right;
			else
				{
				tail->right = (BST *)(malloc(sizeof(BST)));
				tail = tail->right;
				break;
				}

			}

		}

	// If new leaf could not be created, silently drop the item.
	if(tail == NULL)
		return(head);

	// Fill in values for new leaf node.
	tail->x = x;
	tail->y = y;
	tail->l = l;
	tail->left = NULL;
	tail->right = NULL;

	// Return pointer to new head location.
	return(head);

	}

// Perform inorder traversal of binary tree.
void travBST(BST *head, FILE *txt)
	{

	// If no node or no input file, silently cancel operation.
	if((head == NULL) || (txt == NULL))
		return;

	// Recurse for all items to the left.
	travBST(head->left, txt);

	// Create report line for current node.
	fprintf(txt, "%f\t%f\t%f\n", head->x, head->y, head->l);

	// Recurse to right.
	travBST(head->right, txt);

	}

// Create report out of corners.
int cornhole(matrix *lambda, FILE *text)
	{

	// New binary search tree of corners.
	BST *head = NULL;

	// Basic input check.
	if(lambda == NULL)
		{
		trace("cornhole: invalid lambda matrix; fail");
		return(ERROR);
		}

	// Collect all non-zero cells in matrix into search tree.
	int x = lambda->width;
	int y = lambda->height;
	trace("cornhole: building BST from %dx%d matrix.", x, y); 
	for(y = 0; y < lambda->height; y++)
		for(x = 0; x < lambda->width; x++)
			if(*matrix_cell(lambda, x, y) != 0)
				head = insBST(head, x, y, *matrix_cell(lambda, x, y));

	// Generate report from binary search tree.
	trace("cornhole: traversing BST");
	travBST(head, text);

	return(0);

	}
			
