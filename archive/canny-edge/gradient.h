// $Id: gradient.h,v 1.3 2004/03/01 15:07:28 warr Exp $
//
// Gradient Computation Filter Library
// INTERFACE

#ifndef __gaussedge_h
#define __gaussedge_h

#include "matrix.h"
// Function prototypes require "matrix" data type.

matrix *gradient(matrix *, double, bool);
// Returns a matrix containing filtered gradient
// component.  Accepts an image matrix and the value
// of sigma as input, plus a bool which should be false
// to do horizontal (x-axis) gradient, and true for
// y axis.

#endif
