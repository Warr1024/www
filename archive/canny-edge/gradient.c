// $Id: gradient.c,v 1.8 2004/03/02 16:14:48 warr Exp $
//
// Gradient Computation Filter Library
// IMPLEMENTATION

#include <math.h>
#include <stdlib.h>
#include "gradient.h"
#include "matrix.h"
#include "debug.h"

// Function to produce a derivative-of-gaussian 1D mask for filtering.
matrix *gaussmask(int m, double sigma)
	{

	// Create new image matrix of given size m.
	trace("gaussmask: create matrix size %d", m);
	matrix *mtx = matrix_create(m, 1);
	if(mtx == NULL)
		{
		trace("gaussmask: fail");
		return(NULL);
		}

	// Compute coefficiencts for matrix
	trace("gaussmask: filling mask matrix sigma=%f", sigma);
	for(int z = 0; z < m; z++)
		{
		int y = z - 1 - (m >> 1);
		double *x = matrix_cell(mtx, z, 0);
		if(x != NULL)
			*x = -y / (sigma * sigma) * exp(-0.5 * y * y / (sigma * sigma));
		}

	trace("gaussmask: success");
	return(mtx);

	}

// Perform gradient filter in 1D on input image.
matrix *gradient(matrix *inpimg, double sigma, bool yaxis)
	{

	// Safe input checks.
	if(inpimg == NULL)
		{
		trace("gradient: no input image; fail");
		return(NULL);
		}
	if(sigma <= 0)
		{
		oops("Value of sigma too small.");
		trace("gradient: fail");
		return(NULL);
		}

	// Compute gaussian mask size and image discard borders.
	int m = (int)(5 * sigma + 1) | 1;
	int bound = m >> 1;

	// Create gaussian mask.
	trace("gradient: calling gaussmask");
	matrix *mask = gaussmask(m, sigma);
	if(mask == NULL)
		{
		trace("gradient: failed");
		return(NULL);
		}

	// Create the output image space.
	int x = inpimg->width - bound * 2;
	int y = inpimg->height - bound * 2;
	trace("gradient: creating space for output image size=%dx%d", x, y);
	matrix *rtn = matrix_create(x, y);
	if(rtn == NULL)
		{
		trace("gradient: fail");
		return(NULL);
		}

	// Step through each pixel of the image.
	double v;
	trace("gradient: performing convolution on %dx%d matrix", inpimg->width, inpimg->height);
	for(y = bound; y < inpimg->height - bound; y++)
		for(x = bound; x < inpimg->width - bound; x++)
			{

			// Clear previous pixel resultant.
			v = 0;

			// Step through 1D mask.
			for(int p = -bound; p < bound; p++)
				{

				// Find pointer to current location in mask.
				double *a = matrix_cell(mask, p + bound, 0);

				// Find pointer to current location in input image.
				double *b;
				if(yaxis)
					b = matrix_cell(inpimg, x, y + p);
				else
					b = matrix_cell(inpimg, x + p, y);

				// If either cell is invalid, try to skip this iteration.
				if((a == NULL) || (b == NULL))
					continue;

				// Convolve and add to resultant.
				v += (*a) * (*b);

				}

			// Find pointer to location in output image.
			double *c = matrix_cell(rtn, x - bound, y - bound);

			// If location is invalid, try to continue and do the rest of the image.
			if(c == NULL)
				continue;

			// Write convolution result into cell.
			(*c) = v;

			}

	// Return the gradient matrix.
	trace("gaussian: success");
	return(rtn);

	}
