// $Id: nonmax.h,v 1.3 2004/03/02 16:14:48 warr Exp $
// 
// Non-maximum Suppression Module
// INTERFACE

#ifndef __nonmax_h
#define __nonmax_h

#include "matrix.h"
// Need matrix datatype for function prototype.

matrix *nonmax(matrix *, matrix *);
// Perform non-maximum suppresion filter; one matrix
// for magnitude, one for direction in radians.  Returns
// output magnitude, or zero for errors.

#endif
