// $Id: debug.h,v 1.4 2004/03/01 15:07:28 warr Exp $
//
// Debugging and Error Reporting Library
// INTERFACE

#ifndef __debug_h
#define __debug_h

#define ERROR -1
// This is just a non-zero value to return as an error
// code from a function.

int oops(char *fmt, ...);
// Outputs arguments like printf to stderr, guarantees
// output is flushed, then returns ERROR.  To be used
// like return(oops(...));

void trace(char *fmt, ...);
// Outputs a trace statement to stderr, only if compiled
// with DBGTRACE defined.  Used for debugging purposes only.

#endif
