// $Id: threshold.c,v 1.5 2004/03/02 19:28:40 warr Exp $
//
// Hysterisis Thresholding Module
// IMPLEMENTATION

#include <stdlib.h>
#include "matrix.h"
#include "threshold.h"
#include "debug.h"

// This function tries a pixel at a given threshold, then recurses neighbors
// (if it's a hit) with a second threshold.
void trypx(matrix *in, matrix *out, matrix *vis, int x, int y, double thr, double thl)
	{

	// Find cell, silently fail if it doesn't exist.
	double *v = matrix_cell(vis, x, y);
	if(v == NULL)
		return;

	// If it's visited, cancel, else mark it visited.
	if((*v) != 0)
		return;
	(*v) = 1;

	// Try to locate input and output image cells.
	double *i = matrix_cell(in, x, y);
	double *o = matrix_cell(out, x, y);
	if((i == NULL) || (o == NULL))
		return;

	// If the cell is above our threshold...
	if((*i) >= thr)
		{

		// Set the output to 1.
		(*o) = 1;

		// Try its neighbors with the lower threshold.
		trypx(in, out, vis, x + 1, y, thl, thl);
		trypx(in, out, vis, x + 1, y + 1, thl, thl);
		trypx(in, out, vis, x + 1, y - 1, thl, thl);
		trypx(in, out, vis, x, y + 1, thl, thl);
		trypx(in, out, vis, x, y - 1, thl, thl);
		trypx(in, out, vis, x - 1, y + 1, thl, thl);
		trypx(in, out, vis, x - 1, y - 1, thl, thl);
		trypx(in, out, vis, x - 1, y, thl, thl);

		}

	}

// Perform hysterisis thresholding on entire image.
matrix *threshold(matrix *mag, double thh, double thl)
	{

	// Make sure we have valid input.
	trace("threshold: checking input");
	if(mag == NULL)
		return(NULL);

	trace("threshold: creating %dx%d visited matrix", mag->width, mag->height);
	matrix *visit = matrix_create(mag->width, mag->height);
	if(visit == NULL)
		return(NULL);

	// Initialize all cells to unvisited.
	trace("threshold: initializing visitation matrix");
	int x, y;
	for(y = 0; y < visit->height; y++)
		for(x = 0; x < visit->width; x++)
			{

			// Find cell in visited matrix.
			double *v = matrix_cell(visit, x, y);
			if(v == NULL)
				continue;

			// Mark unvisited.
			(*v) = 0;

			}

	// Create output image matrix
	trace("threshold: creating %dx%d output matrix", mag->width, mag->height);
	matrix *rtn = matrix_create(mag->width, mag->height);
	if(rtn == NULL)
		return(NULL);

	// Initialize output to all 0.
	trace("threshold: clearing all values to 0");
	for(y = 0; y < rtn->height; y++)
		for(x = 0; x < rtn->width; x++)
			{

			// Find cell in output matrix.
			double *v = matrix_cell(rtn, x, y);
			if(v == NULL)
				continue;

			// Clear cell (mark no edge).
			(*v) = 0;

			}

	// Perform hysterisis thresholding across image.
	trace("threshold: recursing image");
	for(y = 0; y < rtn->height; y++)
		for(x = 0; x < rtn->width; x++)
			trypx(mag, rtn, visit, x, y, thh, thl);

	trace("threshold: success");
	return(rtn);

	}
