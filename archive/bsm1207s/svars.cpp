/////////////////////////////////////////////////////// Implementation for SVARS

#ifndef _svars_cpp                       // Prevent file from being loaded twice
#define _svars_cpp

#include "svars.h"                             // Include definitions in .H file

svar::svar()                                             // Constructor for svar
{

 defined = var_undeclared;                   // Set variable value to undeclared
 value = 0;                                    // Initialize value, just in case

}

svar *svarlist::list(char x)          // retrieves an svar based on letter value
{

 if((x <= 'z') && (x >= 'a'))       // if letter requested is between a and z...
  return(&(data[x - 'a']));                                   // return the svar

 return(NULL);                                            // otherwise, not svar

}

void svarlist::clear()                      // Clears all variables in this list
{

 for(int x = 0; x < 26; x++)                                 // Each variable...
 {

  data[x].value = 0;                                              // Clear value
  data[x].defined = var_undeclared;                        // Undeclare variable

 }
 
}

#endif                                                                    // EOF
