/******************************************************************************\
*                                                                              *
*                    EXECPIPE.H:  S Code Execution Code                        *
*                       Library for Executing S Code                           *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      SFUNCS program storage library                                          *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      bool execprogram(sfunc *, ostream &): Executes the program stored in    *
*           the pointer from beginning to end, manages its own variables,      *
*           stacks, and calling system.  Returns false if program error.       *
*           All execution output goes to passed ostream.                       *
*                                                                              *
\******************************************************************************/

#ifndef _execpipe_h                   // Prevent this .H from being loaded twice
#define _execpipe_h

#include "bsmglob.h"                       // Include file with global constants
#include "sfuncs.h"                           // For definition of sfunc classes

bool execprogram(sfunc *, ostream &);  // Executes the program passed as pointer

#endif                                                                    // EOF
