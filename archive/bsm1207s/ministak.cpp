//////////////////////////////////////////////////// Implementation for MINISTAK

#ifndef _ministak_cpp                    // Prevent file from being loaded twice
#define _ministak_cpp

#include "ministak.h"                      // Include its functional definitions

ministack::ministack()                          // Constructor makes stack empty
{

 top = 0;

}

bool ministack::isfull()             // Returns true if the stack is full or not
{

 return(top >= ministacksize);             // Check if top is over size of stack

}

bool ministack::isempty()                      // Returns true if stack is empty
{

 return(top == 0);                          // Top of list is at 0 => empty list

}

bool ministack::push(svar s)                      // Pushes an svar to the stack
{

 if(isfull())                                      // Fail if stack already full
  return false;

 data[top] = s;                     // Set data to top of stack, move pointer up
 top++;

 return true;                                            // Operation successful

}

bool ministack::pop(svar &s)                      // Pops an svar from the stack
{

 if(isempty())                                   // If stack is already empty...
  return false;                                                          // fail

 top--;
 s = data[top];                    // Move top down, return element it points to

 return true;                                            // Operation successful

}

#endif                                                                    // EOF
