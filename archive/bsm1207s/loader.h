/******************************************************************************\
*                                                                              *
*                   LOADER.H:  S Program Loading Library                       *
* 		  	  Procedures for loading an S program from file to memory             *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      SFUNCS program storage library                                          *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      sfunc *loadprogram(istream &): a function that will load an S program   *
*              from the input stream passed (behaves just like cin).  The      *
*              input stream is assumed to be open and valid.  The sfunc*       *
*              returned is either a pointer to the first sfunc in a list that  *
*              defines the program, or a NULL pointer if there was an error.   *
*              In the event of errors loading, the loader may display any      *
*              load-time errors directly through cout.  If a NULL pointer is   *
*              returned, the program will not execute.                         *
*                                                                              *
\******************************************************************************/

#ifndef _loader_h                          // Prevent .H from being loaded twice
#define _loader_h

#include "bsmglob.h"                                         // BMS1027S globals
#include "sfuncs.h"                           // For definition of sfunc classes

sfunc *loadprogram(istream &);                  // Function to load an S program

#endif                                                                    // EOF