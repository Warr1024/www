//////////////////////////////////////////////////// Implementation for MEGASTAK

#ifndef _megastak_cpp                    // Prevent file from being loaded twice
#define _megastak_cpp

#include "megastak.h"                      // Include its functional definitions

megastack::megastack()                              // Constructor for megastack
{

 swapfile.open("bsm1207s.swp", ios::in | ios::out | ios::binary);   // Open swap
 swapfile.seekg(0);                   // Set swap file position to start of file

}

megastack::~megastack()                              // Destructor for megastack
{

 swapfile.close();                                        // Close the swap file

}

bool megastack::push(char *data, unsigned int datasize)        // Push operation
{

 if(!swapfile || (swapfile.tellg() >= megastacksize)) // If swapfile not open or
  return false;                                          // Stack overflow, fail

 swapfile.write(data, datasize);               // Write block itself to swapfile

 if(!swapfile)                                // If write failed, fail operation
  return false;

 swapfile.write((char *)(&datasize), sizeof(datasize));      // Write block size

 return true;                                            // Operation successful

}

bool megastack::pop(char *data, unsigned int datasize)          // Pop operation
{

 unsigned int matchsize;                   // Value for comparing size of blocks
 long int cur = swapfile.tellg() - sizeof(datasize);      // Position to seek to

 if(!swapfile || (cur < 0))        // If swapfile error or stack underflow, fail
  return false;

 swapfile.seekg(cur);                     // Seek to position of last block size

 if(!swapfile)                                           // If seek failed, fail
  return false;

 swapfile.read((char *)(&matchsize), sizeof(matchsize));      // Read block size

 if(!swapfile || (matchsize != datasize))      // If read failed, or block sizes
  return false;                                             // don't match, fail

 cur -= datasize;                      // Move back to beginning of actual block
 swapfile.seekg(cur);

 if(!swapfile)                              // Make sure swapfile is still valid
  return false;

 swapfile.read(data, datasize);                         // Read the block itself

 if(!swapfile)
  return false;

 swapfile.seekg(cur);                                // Seek to new top of stack

 return true;                                             // Operation succeeded

}

bool megastack::empty()                        // Returns true if stack is empty
{

 return(!swapfile.tellg());                     // If file is empty, so is stack

}

#endif                                                                    // EOF
