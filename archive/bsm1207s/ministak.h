/******************************************************************************\
*                                                                              *
*            MINISTAK.H:  S Program Execution Micro-Stack Library              *
*     		  	 Class for managing a small-scale stack of svars                *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      SVARS S variable storage library                                        *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      class ministack                                                         *
*                                                                              *
*            bool push(svar): pushes svar to the stack, returns false if the   *
*                 stack was full.                                              *
*                                                                              *
*            bool pop(svar &): pops svar from the stack, returns false if the  *
*                 stack was empty.                                             *
*                                                                              *
*            bool isempty(): reports whether or not the stack is empty         *
*                                                                              *
*            bool isfull(): reports whether or not the stack is full           *
*                                                                              *
\******************************************************************************/

#ifndef _ministak_h                     // Prevents file from being loaded twice
#define _ministak_h

#include "bsmglob.h"                                       // Gloabals used here
#include "svars.h"                                  // S variable ADTs used here

class ministack                       // Ministack class for parser and executor
{

 private:

  svar data[ministacksize];                                     // List of svars
  unsigned int top;                                     // Index of top of array

 public:

  bool push(svar);                                    // Pushed an svar to stack
  bool pop(svar &);                                      // Pops svar from stack
  bool isempty();                              // Returns true if stack is empty
  bool isfull();                                // Returns true if stack is full

  ministack();                                                    // Constructor

};

#endif                                                                    // EOF