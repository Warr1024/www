//////////////////////////////////////////////////// Implementation for EXECPIPE

#ifndef _execpipe_cpp                    // Prevent file from being loaded twice
#define _execpipe_cpp

#include <iostream.h>                                       // For IO operations
#include <stdio.h>                                              // For sprintf()
#include <ctype.h>                                     // for isalpha() and such

#include "execpipe.h"                              // Definitions for the module
#include "ministak.h"                                // Ministack for call/takes
#include "megastak.h"                        // Megastack for main program stack
#include "parser.h"                             // S postorder expression parser
#include "svars.h"                                                // S variables

bool execprogram(sfunc *program, ostream &o)                 // Runs the program
{

 megastack mega;                                               // Main megastack
 ministack mini;                                               // Main ministack
 svarlist heap;                                     // All variables in register
 funcline *curline;                                 // Current line in execution

 char temp[maxstringsize];                              // misc. temporary stuff
 svar tmpsvar;                                          //                    |
 sfunc *newptr;                                         //                    |
 int count;                                             //                  <-+

 if((program != NULL) && program->findfunc("main"))       // If valid program...
  curline = program->findfunc("main")->firstline;     // Set start point to main
 else
 {

  generror(o, " ", 0, "Cannot find function 'main'");
  return false;                                               // Otherwise, fail

 }

 while(curline != NULL)                     // While lines of code to execute...
  switch(curline->cmdtype)
  {

                                                                     // VARIABLE
   case(1): if(!heap.list(curline->lvalues[0]))
            {

             sprintf(temp, "%s%c", "Encountered invalid variable ",
                                                  curline->lvalues[0]);
             generror(o, curline->owner, curline->linenum, temp);
             return false;                   // Invalid definition of a variable

            }
            if(heap.list(curline->lvalues[0])->defined)
            {

             sprintf(temp, "%s%c", "Duplicate definition of variable ",
                                                  curline->lvalues[0]);
             generror(o, curline->owner, curline->linenum, temp);
             return false;                 // Duplicate definition of a variable

            }
            else
             heap.list(curline->lvalues[0])->defined = var_undefined;
                                                         // Declare the variable

            curline = curline->nextline;

            break;

                                                                          // SET
   case(2): if(heap.list(curline->lvalues[0])->defined == var_undeclared)
            {

             sprintf(temp, "%s%c", "Variable not declared ",
                                       curline->lvalues[0]);
             generror(o, curline->owner, curline->linenum, temp);
             return false;         // Tried to set a variable that's not defined

            }
            else
            {

             *(heap.list(curline->lvalues[0]))
                                            = parsemath(heap, curline->rvalues);
                                  //Attempt to parse expression in SET statement

             if(heap.list(curline->lvalues[0])->defined == var_undeclared)
             {

              generror(o, curline->owner, curline->linenum,
                        "Arithmetic / logic parser error");
              return false;                 // Parser failed to parse expression

             }

             if(heap.list(curline->lvalues[0])->defined == var_undefined)
              genwarn(o, curline->owner, curline->linenum, "Result undefined");
                                                   // Warn if operands undefined

            }

            curline = curline->nextline;

            break;

                                                                        // PRINT
   case(3): tmpsvar = parsemath(heap, curline->lvalues);            // Parse the
                                                     // expression to be printed
            if(tmpsvar.defined == var_undeclared)
            {

              generror(o, curline->owner, curline->linenum,
                        "Arithmetic / logic parser error");
              return false;                 // Parser failed to parse expression

            }

            if(tmpsvar.defined == var_undefined)   // Warn if operands undefined
             genwarn(o, curline->owner, curline->linenum, "Result undefined");

            if(tmpsvar.defined == var_undefined)          // Output result value
             o << "Undefined\n";
            else
             o << tmpsvar.value << endl;

            curline = curline->nextline;

            break;

                                                                // CALL_FUNCTION
   case(4): newptr = program->findfunc(curline->lvalues);    // Look up function

            if(newptr == NULL)
            {

              sprintf(temp, "%s%s%s", "Function ", curline->lvalues,
                                                      " not found");
              generror(o, curline->owner, curline->linenum, temp);
              return false;                            // Couldn't find function

            }

            for(count = 0; count < int(strlen(curline->rvalues)); count++)
             if(isalpha(curline->rvalues[count]))  // Push all actual parameters
             {                                               // to the ministack

              if(heap.list(curline->rvalues[count])->defined == var_undeclared)
              {

               sprintf(temp, "%s%c%s%s", "Variable ", curline->rvalues[count],
                   " does not exist, used in call to ", curline->lvalues);
               generror(o, curline->owner, curline->linenum, temp);
               return false;                 // Passing undeclared to a function

              }
              else
               mini.push(*(heap.list(curline->rvalues[count])));

             }

                                              // Push variable heap to megastack
            if(!mega.push((char *)(&heap), sizeof(heap)))
            {

             generror(o, curline->owner, curline->linenum,
                       "Main execution stack corrupted!");
             return false;                                    // Corrupted stack

            }

            heap.clear();                // Clear heap for new function instance

            for(count = strlen(newptr->takes) - 1; count >= 0; count--)
             if(isalpha(newptr->takes[count]))  // Pop formal parameters from
             {                                                  // the ministack

              if(!mini.pop(*(heap.list(newptr->takes[count]))))
              {

               sprintf(temp, "%s%s", "Mismatched parameters in call to ",
                                                       curline->lvalues);
               generror(o, curline->owner, curline->linenum, temp);
               return false;                               // Parameter mismatch

              }

             }

            if(!mini.isempty()) // Parameter mismatch if ministack has leftover
            {

             sprintf(temp, "%s%s", "Mismatched parameters in call to ",
                                                     curline->lvalues);
             generror(o, curline->owner, curline->linenum, temp);
             return false;                                 // Parameter mismatch

            }

                                // Push pointer to next instruction to megastack
            if(!mega.push((char *)(&curline->nextline),
                                              sizeof(curline->nextline)))
            {

             generror(o, curline->owner, curline->linenum,
                         "Main execution stack OVERFLOW");
             return false;                                    // Corrupted stack

            }

            curline = newptr->firstline;       // Continue execution at start of
                                                              // called function

            break;

                                                                 // END_FUNCTION
   case(5): if(mega.empty())              // If stack empty, last function ended
             return true;                                        // Program done

            if(!mega.pop((char *)(&curline), sizeof(curline)))
            {                            // Pop execution pointer from megastack

             generror(o, curline->owner, curline->linenum,
                "Main execution stack corrupted popping code pointer!");
             return false;                                    // Corrupted stack

            }

            if(!mega.pop((char *)(&heap), sizeof(heap)))
            {                                // Pop variable heap from megastack

             generror(o, curline->owner, curline->linenum,
                       "Main execution stack corrupted popping heap!");
             return false;                                    // Corrupted stack

            }


            break;

   default: generror(o, curline->owner, curline->linenum, "Invalid command");
            return false;                                    // Invalid commands

  }

 generror(o, " ", 0, "Function terminated incorrectly");
 return false;           // If run out of lines without normal termination, fail

}

#endif                                                                    // EOF
