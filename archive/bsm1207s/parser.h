/******************************************************************************\
*                                                                              *
*               PARSER.H:  S Post-Order Arithmatic/Logic Parser                *
*     		  	 Function for parsing postorder statements in S                 *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      SVARS S variable storage library                                        *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      svar parsemath(svarlist &, char *): Parses the statement passed as a    *
*           string using the variables in the svarlist, returning an svar      *
*           with the result.  The result will be undefined if the actual       *
*           value cannot be calculated but all the variables are there, and    *
*           undeclared in the event of an error or an undeclared variable.     *
*                                                                              *
\******************************************************************************/

#ifndef _parser_h                       // Prevents file from being loaded twice
#define _parser_h

#include "bsmglob.h"                                       // Gloabals used here
#include "svars.h"                                // Classes for S variable ADTs

svar parsemath(svarlist &, char *);               // Parses postorder expression

#endif                                                                    // EOF
