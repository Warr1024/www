////////////////////////////////////////////////////// Implementation for PARSER

#ifndef _parser_cpp                      // Prevent file from being loaded twice
#define _parser_cpp

#include <ctype.h>                                    // For isdigit() and stuff

#include "parser.h"                        // Include its functional definitions
#include "ministak.h"                                // Ministack used by parser

svar calculate (svar s1, svar s2, char e)           // private function performs
{                                  // operation in e on s1 and s2 (i.e. s1 e s2)

 svar s;                                            // Temp variable for results

 if((s2.defined == var_defined) && (s1.defined == var_defined))       // If both
 {                                                    // operands are defined...

  switch(e)
  {

  	case ('+'): s.defined = var_defined;                              // Addition
             	s.value = s1.value + s2.value;
               return s;

  	case ('-'): s.defined = var_defined;                           // Subtraction
             	s.value = s1.value - s2.value;
               return s;

  	case ('*'): s.defined = var_defined;                        // Multiplication
             	s.value = s1.value * s2.value;
               return s;

  	case ('/'): if(s1.value)
               {

                s.defined = var_defined;                             // Division
                s.value = s1.value / s2.value;

               }
               else
                s.defined = var_undefined;                      // Division by 0

               return s;

  	case ('%'): if(s1.value)
               {

                s.defined = var_defined;                               // Modulo
                s.value = s1.value % s2.value;

               }
               else
                s.defined = var_undefined;                        // Modulo by 0

               return s;

  	case ('|'): s.defined = var_defined;                             // Binary OR
             	s.value = s1.value | s2.value;
               return s;

  	case ('&'): s.defined = var_defined;                            // Binary AND
             	s.value = s1.value & s2.value;
               return s;

  	case ('^'): s.defined = var_defined;                            // Binary XOR
             	s.value = s1.value ^ s2.value;
               return s;

   default:	s.defined = var_undeclared;          // Undefined operation -- error
            return s;

  }

 }

                              // Not(both operands are defined) at this point...
 s.defined = var_undefined;                                  // Assume undefined

 if (s1.defined == var_undeclared || s2.defined == var_undeclared)
  s.defined = var_undeclared;     // If an operand undeclared, result undeclared

 return s;                                                       // return value

}

svar parsemath(svarlist &allvars, char *exprn)          // parses the expression
{

 int temp = 0;                                      // temp for numerical values
 bool flag = false;          // flag indicates that a number was being processed

 ministack m;                                           // ministack for parsing
 svar s;                                                      // temp for return

 for(int pos = 0; exprn[pos] != '\0'; pos++)  // With each char in expression...
 {

  if(isdigit(exprn[pos]))                                 // If it's a number...
  {

   temp = temp * 10 + exprn[pos] - '0';                   // Concatenate to temp
   flag = true;                                 // We're now processing a number

  }
  else
  {

   if(flag)                              // If we were working with a number...
   {

    s.defined = var_defined;               // Put the number into the svar temp
    s.value = temp;

    m.push(s);                                              // Push that number

    temp = 0;                                      // Reset numerical processor
    flag = false;

   }

   if(isalpha(exprn[pos]))                                   // If it's a letter
   {

    if(!(allvars.list(exprn[pos])))          // check, make sure variable valid
    {

     s.defined = var_undeclared;           // Failure case: return an undeclared
     return(s);

    }

    if(m.isfull())                                     // Prevent stack overflow
    {

     s.defined = var_undeclared;           // Failure case: return an undeclared
     return(s);

    }

    m.push(*(allvars.list(exprn[pos])));

	}
   else
    if (exprn[pos] > ' ') // It's not an alphanumeric, so unless it's whitespace
    {                                                        // it's an operator

     svar s2, s1;                                      // Two temps for operands

     if(!m.pop(s2))      // Pop second operand (reverse order 'cuz it's a stack)
     {

      s.defined = var_undeclared;          // Failure case: return an undeclared
      return(s);

     }

     if(!m.pop(s1))                                         // Pop first operand
     {

      s.defined = var_undeclared;          // Failure case: return an undeclared
      return(s);

     }

     if(!m.push(calculate (s1, s2, exprn[pos])))     // Now try to calculate the
     {                            // result of the operation and push the result

      s.defined = var_undeclared;          // Failure case: return an undeclared
      return(s);

     }

    }

   }

 }

 if (m.isempty())                  // Stack shouldn't be empty -- if it is, fail
  s.defined = var_undeclared;
 else
  m.pop(s);                                         // Otherwise, pop the result

 if (!m.isempty())              // If stack is not empty after popping one, fail
  s.defined = var_undeclared;

 return s;                                                  // Return the result

}

#endif                                                                    // EOF
