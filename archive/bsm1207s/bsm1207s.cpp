/******************************************************************************\
*                                                                              *
*           /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\           *
*           \                       ============================== /           *
*           /   Banks                                              \           *
*           \     Suen &                           Version 1.10    /           *
*           /       Moroski CSE                   November, 1999   \           *
*           \         120 Assignment                               /           *
*           /           7                                          \           *
*           \             S Interpreter                   ******** /           *
*           /                                     **************** \           *
*           \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/           *
*                                                                              *
*                 BSM1027S S Computer Language Interpreter v1.00               *
*                    SUPER-ROBUST ERROR TRAPPING AND REPORTING                 *
*                        For UNIX and WinDOS, 16 or 32 bit                     *
*                                                                              *
*                  (C)1999 Aaron Suen, Eric Banks, Jeff Moroski                *
*                                                                              *
\******************************************************************************/

          ////////////            ////////////    ////            ////
          //////////////        //////////////    //////        //////
          ////       /////    /////               ////////    ////////
          ////       /////    /////               ////////////////////
          //////////////        //////////        ////  ////////  ////
          //////////////          //////////      ////    ////    ////
          ////       /////               /////    ////            ////
          ////       /////               /////    ////            ////
          //////////////      //////////////      ////            ////
          ////////////        ////////////        ////            ////





#include "bsmglob.h"                                    // BSM1207S global stuff
#include "sfuncs.h"                        // classes for program code structure
#include "execpipe.h"                       // Functions for executing S program
#include "loader.h"                           // Functions for loading S program

void horizontalbar(ostream &o)                    // Draws horizontal double-bar
{

 for(int x = 0; x < 80; x++)                      // Output 80 =-like characters
  o << char(205);

}

int main()                                                       // MAIN PROGRAM
{

 sfunc *program;                                    // Pointer to S program code

 cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
                                // Cheap way of clearing screen thru iostreaming

 titlebar(cout);                                           // Displays title bar

 horizontalbar(cout);
 cout << "LOADING PROGRAM . . .\n";

 program = loadprogram(cin);                        // Loads program into memory

 if(program == NULL)                       // In the event of loading failure...
 {

  cout << "\nFAILED TO LOAD S PROGRAM\n\nABNORMAL PROGRAM TERMINATION\n\n";
  return(1);                                                             // Fail

 }

 cout << "\nPROGRAM LOADED SUCCESSFULLY\nSTARTING PROGRAM EXE"
      << "CUTION PIPE . . .\n";
 horizontalbar(cout);
 cout << "\n";

 if(!execprogram(program, cout))                    // Run program.  If error...
 {

  cout << "\nFATAL ERROR RUNNING S PROGRAM\n\nABNORMAL PROGRAM TERMINATION\n\n";
  return(1);                                                             // Fail

 }

 cout << "\nProgram terminated normally\n\n";
 return(0);                                                  // The happy ending

}

////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// E O F //
////////////////////////////////////////////////////////////////////////////////
