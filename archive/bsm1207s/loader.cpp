////////////////////////////////////////////////////// Implementation for LOADER

#ifndef _loader_cpp                      // Prevent file from being loaded twice
#define _loader_cpp

#include <iostream.h>                                  // For using stream types
#include <stdio.h>                                          // For using sprintf
#include "loader.h"                                         // Loader definition

bool globalerror = false;   // Global error supercedes encapsulation within this
                  // module -- for reporting errors from child functions without
                             // making an ungodly amount of reference parameters

bool findstart(istream &in)                        // Finds START_FUNCTION in in
{

 char newtoken[maxstringsize];                               // New token buffer
 newtoken[0] = '\0';                                    // newtoken assumed null

 while(!in.eof() && !samestring(newtoken, "START_FUNCTION"))
  in >> newtoken;           // While token isn't START_FUNCTION, input a new one

 return(!in.eof());             // If file ended before a START_FUNCTION, failed

}

void dovariable(istream &in, funcline *f)              // handles VARIABLE lines
{

 in >> f->lvalues;                              // Inputs lvalues to end of line

}

void doset(istream &in, funcline *f)                        // handles SET lines
{

 char fin[maxstringsize];                              // Temp file input string

 in >> f->lvalues >> fin;     // input the variable to set into lvalue, then the
                                                           // "TO" word into fin

 if(!samestring(fin, "TO"))                   // If SET without TO, report error
 {

  generror(cout, f->owner, f->linenum, "SET without TO");
  globalerror = true;                                            // Global error

 }

 readtoeol(in, f->rvalues);                      // Read rvalues -- rest of line

}

void doprint(istream &in, funcline *f)               // handles PRINT statements
{

 readtoeol(in, f->lvalues);                       // read lvalues to end of line

}

void commandswitch(istream &in, funcline *fl, char *cmd);
    // Function prototype to allow docall to use commandswitch and the other way
                               // around -- recursion fix for a logical obstacle

void docall(istream &in, funcline *f)             // handles CALL_FUNCTION lines
{

 char fin[maxstringsize];                          // Temp string for file input

 in >> f->lvalues >> fin;     // Input name of function called and another token

 if(samestring(fin, "WITH"))                       // if next token is "WITH"...
  readtoeol(in, f->rvalues);                      // put rest of line in rvalues
 else     // else, is a function with no parameters, and token is next statement
 {

  funcline *n = new funcline;   // Create a new funcline for this next statement

  n->linenum = f->linenum + 1;                          // Initialize its values
  n->nextline = NULL;

  f->nextline = n;                                // Link it to the current list
  strcpy(n->owner, f->owner);                    // Copy its owner function name

  commandswitch(in, n, fin);        // recursively handle statements in this new
        // list element (the alternative requires some convolution with function
                                                               // return values)

 }

}

                // Given a command statement, controls processing of instruction
void commandswitch(istream &in, funcline *fl, char *cmd)
{

 if(samestring(cmd, "VARIABLE"))                             // handle VARIABLEs
 {

  fl->cmdtype = 1;
  dovariable(in, fl);
  return;

 }

 if(samestring(cmd, "SET"))                                       // handle SETs
 {

  fl->cmdtype = 2;
  doset(in, fl);
  return;

 }

 if(samestring(cmd, "PRINT"))                                   // handle PRINTs
 {

  fl->cmdtype = 3;
  doprint(in, fl);
  return;

 }

 if(samestring(cmd, "CALL_FUNCTION"))                   // handle CALL_FUNCTIONs
 {

  fl->cmdtype = 4;
  docall(in, fl);
  return;

 }

 if(samestring(cmd, "END_FUNCTION"))                     // handle END_FUNCTIONs
 {

  fl->cmdtype = 5;
  return;

 }

 char unrec[maxstringsize];             // If not one of the above, unrecognized
 sprintf(unrec, "%s%s", "Unrecognized token: ", cmd);            // Report error
 generror(cout, fl->owner, fl->linenum, unrec);
 globalerror = true;                                        // Flag global error

}

void loadstatements(istream &in, sfunc *sf)  // Loads statements into a function
{

 funcline *curline = NULL;   // Data structures for building a linked list on to
 funcline *newline;                                           // the sfunc given

 char fline[maxstringsize];

 do
 {

  newline = new funcline;                                     // Make a new line

  if(curline != NULL)                     // If not first line in linked list...
  {

   curline->nextline = newline;                      // Link it to existing list
   newline->linenum = curline->linenum + 1;               // Set its line number

  }
  else
  {

   sf->firstline = newline;          // otherwise first node in list is new line
   newline->linenum = 1;                        // first line number starts at 1

  }

  curline = newline;                              // Now, work with current line
  curline->nextline = NULL;                      // Current line is last in list

  strcpy(curline->owner, sf->name);                    // Set its owner function

  cout << char(250);                              // Little tik markz =) *CHEAT*
  in >> fline;                          // Read in a command to put in this line

  if(samestring(fline, "START_FUNCTION"))    // If function started without last
  {                                                              //one ending...

   generror(cout, sf->name, curline->linenum,                    // report error
           "START_FUNCTION encountered before END_FUNCTION");

   globalerror = true;                                      // Flag global alert

  }

  if(samestring(fline, "TAKES"))                    // Special: if it's takes...
  {

   readtoeol(in, sf->takes);                            // modify owner function
   in >> fline;                       // Reread in a command to put in this line

  }

  commandswitch(in, curline, fline);                         // handle commands

  while(curline->nextline != NULL)   // Because of possiblity of recursion, make
   curline = curline->nextline;      // sure you're working with the end of list

 } while((curline->cmdtype != 5) && !in.eof());                // Continue until
                               // end of file or END_FUNCTION was just processed

 if(curline->cmdtype != 5)                // If function did not end before file
 {

  generror(cout, sf->name, curline->linenum,                     // report error
          "END_FUNCTION not found");

  globalerror = true;                                       // Flag global alert

 }

}

sfunc *loadprogram(istream &in)            // Main program guts -- loads program
{

 sfunc *program = NULL;                     // Pointers to make new program list
 sfunc *curfunc = NULL;
 sfunc *newfunc;

 char temp[maxstringsize];                            // What could "temp" mean?

 cout << '[';                           // Output boxes for Tik Markz =) *CHEAT*

 while(findstart(in))                      // While START_FUNCTIONS are found...
 {

  newfunc = new sfunc;                             // Create a new function link

  if(curfunc != NULL)
   curfunc->nextfunc = newfunc;           // If list exists, link function to it
  else
   program = newfunc;                                   // Otherwise, start list

  curfunc = newfunc;                            // Work with current function...

  cout << char(254);                              // Little tik markz =) *CHEAT*
  in >> curfunc->name;                                         // Input its name

  if(program->findfunc(curfunc->name) != curfunc)              // Look for dupes
  {

   sprintf(temp, "%s%s", "Duplicate definition of function ", curfunc->name);
   genwarn(cout, " ", 0, temp);                        // Output warning message

  }

  loadstatements(in, curfunc);                   // Load its statements in to it

 }

 cout << ']';                           // Output boxes for Tik Markz =) *CHEAT*

 if(program == NULL)            // If no functions loaded, error -- null program
  generror(cout, " ", 0, "Could not find START_FUNCTION - Program blank?");

 if(globalerror)     // If global error occurred, program failed to load
 {

  delete program;    // Try to clean up aborted program (not sure if it does...)
  program = NULL;                                      // Return failure to load

 }

 return program;                           // Return what was loaded for program

}

#endif                                                                    // EOF
