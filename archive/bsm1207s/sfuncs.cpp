////////////////////////////////////////////////////// Implementation for SFUNCS

#ifndef _sfuncs_cpp                 // Prevent .CPP file from being loaded twice
#define _sfuncs_cpp

#include "sfuncs.h"                         // Include specifications in .H file

sfunc::sfunc()                                     // sfunc standard constructor
{

 name[0] = '\0';                                        // Initialize all values
 takes[0] = '\0';
 firstline = NULL;
 nextfunc = NULL;

}

sfunc::~sfunc()                                      // sfunc special destructor
{

 if(firstline)                     // Recursively destroy lines in this function
  delete firstline;

 if(nextfunc)                           // Destroy all other functions down list
  delete nextfunc;

}

sfunc *sfunc::findfunc(char *tofind)                 // Finds a function by name
{

 if(samestring(tofind, name))                    // If this is it, return itself
  return this;

 if(nextfunc)                                   // If more to search, recurse it
  return nextfunc->findfunc(tofind);
 else
  return NULL;                                           // Otherwise, not found

}

funcline::funcline()                       // Function line standard constructor
{

 linenum = 0;                       // Initalize all variables to default values
 cmdtype = 0;
 lvalues[0] = '\0';
 rvalues[0] = '\0';
 nextline = NULL;

}

funcline::~funcline()                         // Special destructor for funcline
{

 if(nextline)                         // Delete recursively lines after this one
  delete nextline;

}

#endif                                                                    // EOF
