/******************************************************************************\
*                                                                              *
*                   SFUNCS.H:  S Function Storage Library                      *
*   			     Classes for storage of S program in memory                    *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      none                                                                    *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      class sfunc                                                             *
*                                                                              *
*         char name[]: a string that should hold the name of the function.     *
*                     The S language can be assumed to be case-sensitive like  *
*                     C++, so preserve the function name's case.               *
*                                                                              *
*         char takes[]: a string that contains the list of variables the       *
*                     function takes as parameters.  All characters in the     *
*                     string should be either lower-case letters or blank      *
*                     spaces (the spaces will be ignored).                     *
*                                                                              *
*         funcline *firstline: a pointer that should point to the first        *
*                     executible line of S code in the function.  This pointer *
*                     MUST be defined - the shortest possible function in S    *
*                     must have at least one line: END_FUNCTION.  See the      *
*                     funcline class for info on function lines.               *
*                                                                              *
*         sfunc *nextfunc: a pointer to another sfunc in a linked list.  If    *
*                     there are more functions, nextfunc must point to the     *
*                     next one in the list.  If there are no more, nextfunc    *
*                     MUST be equal to NULL.                                   *
*                                                                              *
*         sfunc *findfunc(char *): a function that returns a pointer to the    *
*                     function in the list whose name matches the char*        *
*                     passed, or a NULL if not found.  Note that this function *
*                     can only search further down the list.                   *
*                                                                              *
*      class funcline                                                          *
*                                                                              *
*         constructor: standard only                                           *
*                                                                              *
*         unsigned int linenum: a positive integer indicating the line number  *
*                     within the function.  Lines are numbered from 1 up,      *
*                     separately within each function.  Blank lines are        *
*                     skipped and not stored in memory.                        *
*                                                                              *
*         unsigned char cmdtype: a small positive integer indicating the       *
*                     command on that line.  Values are:                       *
*                          0: Reserved (will cause error)                      *
*                          1: VARIABLE                                         *
*                          2: SET                                              *
*                          3: PRINT                                            *
*                          4: CALL_FUNCTION                                    *
*                          5: END_FUNCTION                                     *
*                         6+: Reserved (will cause error)                      *
*                     Reserved value 0 is placed to verify the program was     *
*                     read correctly, and reserved values 6+ are in case of    *
*                     future additions.                                        *
*                     Note that there are no values for blank lines or for     *
*                     BEGIN_FUNCTION lines, so they must not be stored, but    *
*                     there IS an entry for END_FUNCTION, which should be      *
*                     stored.                                                  *
*                                                                              *
*         char lvalues[]: a string that contains the left-hand values from a   *
*                     command.  Valid lvalues depend on the function:          *
*                          VARIABLE: a string of lower-case letters or blank   *
*                             spaces to be defined.  All variables in the list *
*                             will be defined together (expanded feature).     *
*                          SET: a single lower-case character whose value is   *
*                             to be set (the variable between SET and TO).     *
*                          PRINT: any expression to the right of a PRINT       *
*                             statement, which will be sent to the postorder   *
*                             parser and outputted after evaluation (expanded  *
*                             feature).                                        *
*                          CALL_FUNCTION: the name of the function to be       *
*                             called, with matching case.                      *
*                          END_FUNCTION: (none)                                *
*                                                                              *
*         char rvalues[]: a string that contains the right-hand values from a  *
*                     command.  Valid rvalues depend on the function:          *
*                          VARIABLE: (none)                                    *
*                          SET: any expression to the right of TO that will be *
*                             evaluated and stored in the variable in lvalue.  *
*                          PRINT: (none)                                       *
*                          CALL_FUNCTION: the list of variables, matching in   *
*                             order the variables in the TAKES of the function *
*                             being called.  Blanks are ignored.               *
*                          END_FUNCTION: (none)                                *
*                                                                              *
*         char owner[]: the name of the S function that owns this line of code *
*                                                                              *
\******************************************************************************/

#ifndef _sfuncs_h                     // Prevent this .H from being loaded twice
#define _sfuncs_h

#include "bsmglob.h"                       // Include file with global constants

class funcline                             // Class for a line within a function
{

 public:

  unsigned int linenum;                                   // Line number counter
  unsigned char cmdtype;                                      // Type of command
  char lvalues[maxstringsize];                             // lvalues of command
  char rvalues[maxstringsize];                             // rvalues of command
  char owner[maxstringsize];                     // function that owns this line
  funcline *nextline;                                   // Next line in function

  funcline();                                                     // Constructor
  ~funcline();                                                     // Destructor

};

class sfunc                                           // Class for an S function
{

 public:

  char name[maxstringsize];                                  // Name of function
  char takes[maxstringsize];                    // Formal parameter declarations
  funcline *firstline;                                  // Pointer to first line
  sfunc *nextfunc;                                   // Pointer to next function

  sfunc();                                                        // Constructor
  ~sfunc();                                                        // Destructor

  sfunc *findfunc(char *);                           // Finds a function by name

};

#endif                                                                    // EOF
