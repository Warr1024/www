/******************************************************************************\
*                                                                              *
*                   SVARS.H:  S Function Storage Library                      *
*   			     Classes for storage of S program in memory                    *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      none                                                                    *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      class svar                                                              *
*                                                                              *
*         unsigned char defined: takes on these values:                        *
*              var_undeclared: variable has not been declared yet              *
*              var_undefined: variable declared, but value not set yet         *
*              var_defined: variable defined, all OK                           *
*                                                                              *
*         long int value: the actual value stored in the variable, if it's     *
*              defined                                                         *
*                                                                              *
*      class svarlist                                                          *
*                                                                              *
*         svar *list(char): looks up a variable in a list of variables         *
*              referenced by name (a lower case letter), returns a pointer     *
*              to the item itself                                              *
*                                                                              *
*         void clear(): Undeclares and resets all svars in list                *
*                                                                              *
\******************************************************************************/

#ifndef _svars_h                      // Prevent this .H from being loaded twice
#define _svars_h

#include "bsmglob.h"                       // Include file with global constants

#define var_undeclared 0                          // constants for svar::defined
#define var_undefined 1
#define var_defined 2

class svar                             // class for a variable in the S language
{

 public:

  unsigned char defined;                                     // declared/defined
  long int value;                                           // value of variable

  svar();                                                         // constructor

};

class svarlist                           // list of all variables for a function
{

 private:

  svar data[26];                             // 26 variables (1 for each letter)

 public:

  svar *list(char);                         // converts letter to an actual svar
  void clear();                              // Undeclares all variables in list

};

#endif                                                                    // EOF