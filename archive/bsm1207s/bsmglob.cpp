///////////////////////////////////////////////////// Implementation for BSMGLOB

#ifndef _bsmglob_cpp                     // Prevent file from being loaded twice
#define _bsmglob_cpp

#include <stdio.h>        // For sprintf, used to convert and concatenate values
#include "bsmglob.h"                              // Definitions for this module

    // PRIVATE function, produces a general universal alert, used by genwarn and
                                       // generror, is the guts of the procedure
void genalert(ostream &ou, char *func, int lnum, char *what, char *etype)
{

 char l0[maxstringsize];                // Four strings for four lines of output
 char l1[maxstringsize];
 char l2[maxstringsize];
 char l3[maxstringsize];


 if(lnum)                               // String together second line of report
  sprintf(l1, "%s%s%s%s%i%c", etype, "in function ", func,
                                 " in statement ", lnum, ':');
 else
  sprintf(l1, "%s%s", etype, "in current process:");

 sprintf(l2, "%s%s", "  ", what);         // String together last line of report

 int mlen = strlen(l1);             // Find which line is the longest to fit box
 if(int(strlen(l2)) > mlen)
  mlen = strlen(l2);

 mlen += 2;                                  // Add room around text for borders

 for(int x = 0; x < mlen; x++)                                    // Prepare box
 {

  if(x > int(strlen(l1)) - 1)                                // Clean out line 2
  {

   l1[x] = ' ';
   l1[x + 1] = 0;

  }

  if(x > int(strlen(l2)) - 1)                                // Clean out line 3
  {

   l2[x] = ' ';
   l2[x + 1] = 0;

  }

  l3[x] = 196;                                                // Horizontal line

 }

 strcpy(l0, l3);                                      // Copy last line to first

 l0[0] = 218;                               // Put left corners and sides on box
 l1[0] = 179;
 l2[0] = 179;
 l3[0] = 192;

 l0[mlen - 1] = 191;                                        // Finish right side
 l1[mlen - 1] = 179;
 l2[mlen - 1] = 179;
 l3[mlen - 1] = 217;

 l0[mlen] = 0;                                         // Null terminate strings
 l1[mlen] = 0;
 l2[mlen] = 0;
 l3[mlen] = 0;

                                                  // Output the finished product
 ou << endl << l0 << endl << l1 << endl << l2 << endl << l3 << endl;

}

                                             // Reports a general run-time error
void generror(ostream &o, char *func, int lnum, char *what)
{

                           // call genalert (guts) to output fatal error message
 genalert(o, func, lnum, what, "  FATAL ERROR ");

}

                                           // Reports a general run-time warning
void genwarn(ostream &o, char *func, int lnum, char *what)
{

                               // call genalert (guts) to output warning message
 genalert(o, func, lnum, what, "  Warning ");

}

       // Compares two strings x and y, returns true if they are exacty the same
bool samestring(char *x, char *y)
{

 int a;

 for(a = 0; (x[a] != '\0') && (y[a] != '\0'); a++)               // Each char...
  if(x[a] != y[a])                                     // If they don't match...
   return false;                                                        // False

 return(x[a] == y[a]);                            // Make sure terminators match

}

               // Reads a line from in, including whitespace, until \r character
void readtoeol(istream &in, char *str)
{

 int count = 0;                                                       // Counter
 char newchar;                                 // Fresh characters from instream

 do
 {

  in.get(newchar);                                      // Get another character
  str[count] = newchar;                                      // Add it to string

  count++;                                                  // Increment counter

 } while((newchar != 10) && !in.eof());            // Keep it up until CR or EOF

 str[count = '\0'];                                    // Put on null terminator

}

void titlebar(ostream &cou)                        // Displays program title bar
{

cou << "             /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/"
    << "\\/\\/\\/\\/\\/\\/\\/\\/\\\n             \\                       ====="
    << "========================= /\n             /   Banks                    "
    << "                          \\\n             \\     Suen &               "
    << "            Version 1.10    /\n             /       Moroski CSE        "
    << "           November, 1999   \\\n             \\         120 Assignment "
    << "                              /\n             /           7            "
    << "                              \\\n             \\             S Interpr"
    << "eter                   ******** /\n             /                      "
    << "               **************** \\\n             \\/\\/\\/\\/\\/\\/\\/"
    << "\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\n\n";

}

#endif                                                                    // EOF