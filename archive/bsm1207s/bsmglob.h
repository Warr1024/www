/******************************************************************************\
*                                                                              *
*                     BSMGLOB.H:  S Interpreter Globals                        *
*                  Constants and Functions for Global Use                      *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      none                                                                    *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      maxstringsize: a constant for the maximum size of a string throughout   *
*          the program - change to fit needs of the program build.             *
*                                                                              *
*      megastacksize: the maximum size in bytes of the mega stack swap file on *
*          the hard drive.  May depend on the resources availible.             *
*                                                                              *
*      ministacksize: the maximum size in variables of the mini stacks for     *
*          passing variables and parsing arithmetic                            *
*                                                                              *
*      bool samestring(char *, char *): Compares the two strings passed as     *
*          parameters and returns true if they are exactly the same.           *
*                                                                              *
*      void readtoeol(istream &, char *): Reads from the input stream until    *
*          the end of the line (since getline doesn't seem to always work)     *
*                                                                              *
*      void generror(ostream &, char *, int, char *): Outputs general errors   *
*          given function and line number                                      *
*                                                                              *
*      void genwarn(ostream &, char *, int, char *): Outputs general warnings  *
*          given function and line number                                      *
*                                                                              *
*      void titlebar(): Displays the spiffy title bar text message             *
*                                                                              *
\******************************************************************************/

#ifndef _bsmglob_h                       // Prevent file from being loaded twice
#define _bsmglob_h

#define maxstringsize 1024                           // Maximum size of a string
#define megastacksize 268435456                        // Maximum swap file size
#define ministacksize 1024                        // Maximum variable stack size

#include <iostream.h>                                   // For stream operations

bool samestring(char *, char *);                             // Compares strings
void readtoeol(istream &, char *);                // Reads until the end of line

void generror(ostream &, char *, int, char *);                   // Error report
void genwarn(ostream &, char *, int, char *);                  // Warning report

void titlebar(ostream &);                          // Displays program title bar

#endif                                                                    // EOF
