/******************************************************************************\
*                                                                              *
*                MEGASTAK.H:  Massive Swap-File Untyped Stack                  *
*   			     Classes for storage of S program in memory                    *
*                                                                              *
* Dependencies:                                                                *
*                                                                              *
*      none                                                                    *
*                                                                              *
* Interface:                                                                   *
*                                                                              *
*      class megastack                                                         *
*                                                                              *
*              ***** ONLY ONE INSTANCE POSSIBLE APPLICATION-WIDE! *****        *
*                                                                              *
*         bool push(char *, unsigned int): Pushes the block of memory (pointed *
*                     to by the char *, length indicated by the unsigned int)  *
*                     to the megastack swapfile.                               *
*                                                                              *
*         bool pop(char *, unsigned int): Pops the block of memory (pointed    *
*                     to by the char *, length indicated by the unsigned int)  *
*                     from the megastack swapfile, returns an error if the     *
*                     requested and actual block sizes don't match.            *
*                                                                              *
*         bool empty(): Returns true if the stack is currently empty.          *
*                                                                              *
*  NOTE: PROFESSOR SHAFFER GIVES US EXPLICIT PERMISSION TO USE THE SWAP FILE   *
*  FOR STACK OPERATIONS AS A SPECIAL EXCEPTION.                                *
*                                                                              *
\******************************************************************************/

#ifndef _megastak_h                   // Prevent this .H from being loaded twice
#define _megastak_h

#include <fstream.h>                           // Binary file stream definitions
#include "bsmglob.h"                       // Include file with global constants

class megastack                                      // Class for swapfile stack
{

 private:

  fstream swapfile;                                                 // Swap file

 public:

  megastack();                      // Constructor & destructor manage swap file
  ~megastack();

  bool push(char *, unsigned int);                     // Push anything to stack
  bool pop(char *, unsigned int);                       // Pop anything to stack

  bool empty();                                        // True if stack is empty

};

#endif                                                                    // EOF
