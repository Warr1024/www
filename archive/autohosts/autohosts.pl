#!/usr/bin/perl -w
# $Id: autohosts.pl,v 1.12 2007/03/01 04:08:22 warr Exp $
use IO::KQueue;

################################################################################
## USER-CONFIGURABLE SETTINGS
################################################################################

# This is the location of the "hostlist" file, which contains a list of
# hostnames, IP addresses, and MAC addresses assigned manually by a network
# administrator.
$hostlist = '/etc/hostlist';

# This is the location of the "/etc/hosts" file, which contains a list of
# hostname and IP address pairs, for quick lookup bypassing the DNS server.
# This file is updated by autohosts because you may want to use symbolic
# hostnames in firewall rules or other places before the DNS server loads.
$etchosts = '/etc/hosts';

# This is the location of the "dhcpd.conf" file, which contains the
# configuration of ISC's DHCP daemon.  autohosts will add hostname/IP/MAC
# entries to this file as required, in order to implement statically-assigned
# IP addresses by MAC.
$dhcpdconf = '/etc/dhcpd.conf';

# This is the location where dhcpd stores its leases (IP addresses assigned
# on a non-permanent basis).  autohosts will capture any hostname/IP/MAC's
# from this file and add them to the DNS zones automatically (so you DON'T
# need a statically-assigned IP to have a symbolic hostname) and also make
# them viewable in a comment block in the hostlist file.
$dhcpleases = '/var/db/dhcpd.leases';

# This is the location of OpenVPN's status file, which contains a list of
# VPN IP's and X.509 Common Names, which we will use as DNS hostnames and
# insert into the DNS zone files, so OpenVPN nodes are addressable by
# symbolic hostname.
$openvpnstatus = '/var/db/openvpn-status';

# These are the two DNS zone files.  zonea is the forward zone (contains A
# records) and zoneptr is the reverse zone (contains PTR records).  These will
# be updated with any hostname/IP pairs found while searching through any
# of the above files.
$zonea = '/var/named/master/zone.a';
$zoneptr = '/var/named/master/zone.ptr';

# This an optional suffix to be appended to a hostname to create a FQDN.  It
# will be used to create FQDN's as required by etchosts and the DNZ zone files.
$lansuffix = '.szcat.lan';

################################################################################
## AUTOHOSTS UPDATE MAIN PROCESS
################################################################################

# Close stderr and stdout; the process will run as a daemon, producing no output
# to the console.  We don't actually fork into background until after the first
# cycle; thus this app can be run at system startup, and we can be pretty sure
# that files are in a consistent state when the command line returns.
close(STDERR);
close(STDOUT);

# The application runs in a giant infinite loop (via a conditional goto at the
# end of the script).
RESTART:

## -----------------------------------------------------------------------------
## Collect Hostname/IP/MAC Information from Sources
## -----------------------------------------------------------------------------

# Reset the dynamic host list.
%dynalist = ();

# Get the current date in the same format as dhcpd.leases.  The date values are
# in descending order, so this is a "sortable/comparable" date and we can use
# it to determine if the lease has expired by comparing with the lease end date.
$curdate = `date -u '+%Y/%m/%d %H:%M:%S'`;
chomp $curdate;

# Scan any DHCP temporarily-assigned leases in the lease file.  This step is
# done first because these are the lowest priority (if a static-DHCP or VPN
# host with the same name exists, it overwrites this one).
$ends = $host = $ip = $mac = "";
if($dhcpleases and open(INFILE, "< $dhcpleases"))
	{
	while(<INFILE>)
		{
		# Capture the IP address from the lease start line.
		m/lease ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/ and $ip = $1 and next;

		# Capture the ethernet MAC address from the "hardware
		# ethernet" line inside the lease definition.
		m/hardware ethernet ([0-9A-Fa-f:]+)/ and $mac = "$1" and next;

		# Capture the hostname that was sent by the DHCP client.
		# This may contain some invalid characters, and will have
		# to be sanitized later.
		m/client-hostname "(.+)"/ and $host = "$1" and next;

		# Capture lease end time, so we can filter out exired
		# DHCP leases from the host list.
		m|ends\s[0-9]\s([0-9:/\s]+);$| and $ends = "$1" and next;

		# A line beginning with a curly-brace indicates the end
		# of a lease definition.
		if(m/^}/)
			{
			# If at least an IP and hostname have been gathered
			# so far, add them to the memory list.
			if($ip and $host and $ends ge $curdate)
				{
				# Hostname may need to be cleaned up some.
				# Ensure it only contains lower-case letters,
				# numbers, and some non-distruptive special
				# characters.
				$host =~ tr/A-Z/a-z/;
				$host =~ tr/a-z0-9_-//cd;

				# MAC was already sanitized well enough by the
				# regex that captured it; just lowercase it.
				$mac =~ tr/A-F/a-f/;

				# Add the host's IP and MAC to the internal
				# memory list.
				$dynalist{$host}{ip} = $ip;
				$dynalist{$host}{mac} = $mac;
				}

			# Clear all lease variables for the next lease entry.
			$ends = $host = $ip = $mac = "";
			}
		}

	# Close the dhcpd.leases file when we're done.
	close(INFILE);
	}

# Scan through the administrator's static host list next.  This is a higher
# priority than the DHCP leases (entries here will clobber those) but lower
# than the VPN hosts; we assume the administrator is also in control of VPN
# certificates and address assignments, and this allows a host with a static
# address internally to ALSO be a possibly road warrior (good for laptops).
$host = $ip = $mac = "";
if($hostlist and open(INFILE, "< $hostlist"))
	{
	while(<INFILE>)
		{
		# Strip off any comments.
		s/#.*//;

		# If the line contains a hostname and IP address, and possibly
		# a MAC address, all separated by whitespace, process it.
		if(m/([^\s]+)\s+([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)(\s([A-Fa-f0-9:]+))?/)
			{
			# Pull the hostname and sanitize it the same way we
			# did with the DHCP leases; only allow lowercase
			# letters, numbers, and certain punctuation.
			$host = $1;
			$host =~ tr/A-Z/a-z/;
			$host =~ tr/a-z0-9_-//cd;

			# Try to pull the MAC address, if it was captured,
			# otherwise use blank.  Lowercase it.
			(defined $4 and $4 and $mac = $4) or $mac = "";
			$mac =~ tr/A-F/a-f/;

			# Assign the IP (pre-sanitized by the regex) and the
			# MAC to the host.
			$dynalist{$host}{ip} = $2;
			$dynalist{$host}{mac} = $mac;
			}
		}

	# Close the hostlist file.
	close(INFILE);
	}

# Finally, read in the VPN status file written by OpenVPN.  This contains a list
# of hostnames (X.509 certs' Common Names) and associated virtual IP addresses.
# Assimilate this information on top of the existing running host list.
if($openvpnstatus and open(INFILE, "< $openvpnstatus"))
	{
	while(<INFILE>)
		{
		# Look for the header line that indicates the beginning of the
		# CN<->IP table block.  Turn table parsing on when we find it.
		m/^Virtual Address,Common Name/ and $parse = 1 and next;

		# Turn table parsing off when we reach the end of the CN<->IP part.
		m/^GLOBAL STATS/ and $parse = "";

		# While table parsing is on, try to capture a hostname and VPN
		# IP address from the table.
		if($parse and m/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+),([^,]+),/)
			{
			# Sanitize the host as per our standard.  Insert the entry
			# into the table, with no MAC address.
			$host = $2;
			$host =~ tr/A-Z/a-z/;
			$host =~ tr/a-z0-9_-//cd;
			$dynalist{$host}{ip} = $1;
			$dynalist{$host}{mac} = "";
			}
		}

	# Close the VPN status file.
	close(INFILE);
	}

# Create a sorted list of keys of the dynamic host list.  This way, we can
# traverse the hostlist in alphabetical hostname order without having to
# re-sort every time.
@dynahosts = sort keys %dynalist;

## -----------------------------------------------------------------------------
## Rewrite/Update Host Lists, Configs, and Zone Files
## -----------------------------------------------------------------------------

# Checksum the DHCP and DNS config/zone files before rewriting, so later we can
# compare checksums and detect whether the rewritten content is actually
# different than the old content.  We will need to have these files reloaded
# if there are any actual differences.
$dhcpsum = `cksum $dhcpdconf`;
$dnssum = `cksum $zonea $zoneptr`;

# Rewrite the hostlist file, replacing the dynamic host list section at the
# bottom.  This section allows an admin to connect a computer to the network and
# assign it a dynamic IP in order to capture its MAC, then copy'n'paste from the
# dynamic list into the static list to assign it its permanent IP.
if(open(OUTFILE, "> $hostlist.new") and flock(OUTFILE, 2)
	and open(INFILE, "< $hostlist") and flock(INFILE, 2))
	{

	# Copy the hostlist file to the new output file, stripping off any lines
	# that contain #: comments.  I used to keep a copy of the pre-comment-
	# stripped file in memory, but I have to do it this way in case it was
	# changed since the last time the file was opened; it's only safe here
	# because the file is flocked.
	while(<INFILE>) { m/^#:/ or print OUTFILE; }

	# Write the header for the new dynamic host list, explaining what this section
	# of the file means and how it's laid out.
	print OUTFILE "#: ---------------------------------------------------------------------\n";
	print OUTFILE "#: ========================= DYNAMIC HOST LIST =========================\n";
	print OUTFILE "#: ---------------------------------------------------------------------\n";
	print OUTFILE "#: This is the list of all hosts detected on the local network,\n";
	print OUTFILE "#: including all hosts listed above, all currently-connected VPN nodes,\n";
	print OUTFILE "#: and any dynamically-assigned DHCP addresses with hostnames.  This\n";
	print OUTFILE "#: list (and any lines beginning with \"#:\") will be overwritten by a\n";
	print OUTFILE "#: new list any time the network data is updated.\n";
	print OUTFILE "#: ---------------------------------------------------------------------\n";
	print OUTFILE "#: HostName\tIP Address\tMAC Address\n";
	print OUTFILE "#: --------\t----------\t-----------\n";

	# Now, write the dynamic host list table out.  I used tabs to make this
	# fit into columns nicely, though we need extra tabs in certain places
	# (e.g. if the tab size is 8 chars and the hostname is less than 5 chars),
	# though this is by no means a thorough effort.  I only format this file
	# nicely because it's most likely to need to be human-readable.
	for $host ( @dynahosts )
		{
		print OUTFILE "#: $host" . ((length($host) < 5) ? "\t" : "")
			. "\t$dynalist{$host}{ip}\t$dynalist{$host}{mac}\n";
		}

	# Move the output file over top of the original.  Don't release any locks
	# until after the rewrite is done.  This should prevent any issues with a
	# user editing the file while we're trying to run, so long as they don't
	# hold the file locked TOO long...
	if(!rename("$hostlist.new", "$hostlist"))
		{ sleep(1); goto RESTART; }
	close(OUTFILE);
	close(INFILE);
	}
else
	{ sleep(1); goto RESTART; }

# Rewrite the /etc/hosts file, so firewall rules and any daemons that start up before
# the DNS server (or would have to remain running when DNS is down) can use symbolic
# hostnames.  Any content between HOSTLIST and ENDLIST lines will be replaced with
# an actual list of host definitions that matches the HOSTLIST line's IP mask regex.
# In general, I use a mask of "." (include all) in a single block for this file, but
# other configurations are also possible.
$lead = $rl = "";
if(open(INFILE, "< $etchosts") and flock(INFILE, 2)
	and open(OUTFILE, "> $etchosts.new") and flock(OUTFILE, 2))
	{
	while(<INFILE>)
		{
		# If no HOSTLIST regex is defined, we're not in the middle of a 
		# HOSTLIST block, so just copy input to output.  If we're in a
		# HOSTLIST block, ignore input until ENDLIST is reached.
		$rl or print OUTFILE;

		# If a #: HOSTLIST line is encountered, copy the IP match regex.
		# Also, copy the "lead-up" (anything before the #:) because we'll
		# rewrite it on every line in the HOSTLIST block, including the
		# ENDLIST line, thus causing everything to be nicely indented.
		m/^(.*)#: HOSTLIST\s+(.*)/ and $rl = $2 and $lead = $1;

		# When an ENDLIST line is encountered, ouput the new block contents
		# before writing out a fresh ENDLIST line, all indented the same
		# as the HOSTLIST line.  Ignore ENDLIST's that happen before a
		# corresponding HOSTLIST line.
		if($rl and m/#: ENDLIST/)
			{
			# Write a line containing the IP, FQDN, and short hostname
			# into the hosts file for every entry whose IP matches the
			# mask regex.
			for $host ( @dynahosts )
				{
				defined $dynalist{$host}{ip}
					and $dynalist{$host}{ip} =~ m/$rl/ and
					print OUTFILE "${lead}$dynalist{$host}{ip}"
						. "\t$host$lansuffix\t$host\n";
				}

			# Write the indented ENDLIST line.
			print OUTFILE "$lead#: ENDLIST\n";

			# Reset the HOSTLIST regex, marking us as no longer being
			# inside a HOSTLIST block, causing I->O copying to resume.
			$lead = $rl = "";
			}
		}

	# Move the output file over top of the original.  Don't release any locks
	# until after the rewrite is done.  This should prevent any issues with a
	# user editing the file while we're trying to run, so long as they don't
	# hold the file locked TOO long...
	if(!rename("$etchosts.new", "$etchosts"))
		{ sleep(1); goto RESTART; }
	close(OUTFILE);
	close(INFILE);
	}
else
	{ sleep(1); goto RESTART; }

# Rewrite the ISC dhcpd config file.  Look for HOSTLIST/ENDLIST blocks in #: comment lines
# and replace the content between them with lists of host/IP/MAC definitions for any IP's
# that match their regexes and have associated MAC addresses.  This is how the static IP's
# from the hostlist file are copied into the dhcpd.conf file to be implemented.
$lead = $rl = "";
if(open(INFILE, "< $dhcpdconf") and flock(INFILE, 2)
	and open(OUTFILE, "> $dhcpdconf.new") and flock(OUTFILE, 2))
	{
	while(<INFILE>)
		{
		# If no HOSTLIST regex is defined, we're not in the middle of a 
		# HOSTLIST block, so just copy input to output.  If we're in a
		# HOSTLIST block, ignore input until ENDLIST is reached.
		$rl or print OUTFILE;

		# If a #: HOSTLIST line is encountered, copy the IP match regex.
		# Also, copy the "lead-up" (anything before the #:) because we'll
		# rewrite it on every line in the HOSTLIST block, including the
		# ENDLIST line, thus causing everything to be nicely indented.
		m/^(.*)#: HOSTLIST\s+(.*)/ and $rl = $2 and $lead = $1;

		# When an ENDLIST line is encountered, ouput the new block contents
		# before writing out a fresh ENDLIST line, all indented the same
		# as the HOSTLIST line.  Ignore ENDLIST's that happen before a
		# corresponding HOSTLIST line.
		if($rl and m/#: ENDLIST/)
			{
			# Write out a host/IP/MAC definition useable by the ISC dhcpd
			# program, for each host that has both an IP and MAC.  In each
			# block, write the IP's that match the regex mask defined in
			# the HOSTLIST header line.
			for $host ( @dynahosts )
				{
				defined $dynalist{$host}{ip} and $dynalist{$host}{mac}
					and $dynalist{$host}{ip} =~ m/$rl/ and
					print OUTFILE "${lead}host $host { hardware ethernet "
					. "$dynalist{$host}{mac}; fixed-address $dynalist{$host}{ip}; }\n";
				}

			# Write the indented ENDLIST line.
			print OUTFILE "$lead#: ENDLIST\n";

			# Reset the HOSTLIST regex, marking us as no longer being
			# inside a HOSTLIST block, causing I->O copying to resume.
			$lead = $rl = "";
			}
		}

	# Move the output file over top of the original.  Don't release any locks
	# until after the rewrite is done.  This should prevent any issues with a
	# user editing the file while we're trying to run, so long as they don't
	# hold the file locked TOO long...
	if(!rename("$dhcpdconf.new", "$dhcpdconf"))
		{ sleep(1); goto RESTART; }
	close(OUTFILE);
	close(INFILE);
	}
else
	{ sleep(1); goto RESTART; }

# Rewrite the forward DNS zone with all entries that have an IP address.  This
# way, all DHCP static AND dynamic hosts, PLUS any VPN nodes will be addressable
# by symbolic hostname by any host using the DNS server here.
$lead = $rl = "";
if(open(INFILE, "< $zonea") and flock(INFILE, 2)
	and open(OUTFILE, "> $zonea.new") and flock(OUTFILE, 2))
	{
	while(<INFILE>)
		{
		# If no HOSTLIST regex is defined, we're not in the middle of a 
		# HOSTLIST block, so just copy input to output.  If we're in a
		# HOSTLIST block, ignore input until ENDLIST is reached.
		$rl or print OUTFILE;

		# If a ;- HOSTLIST line is encountered, copy the IP match regex.
		# Also, copy the "lead-up" (anything before the ;-) because we'll
		# rewrite it on every line in the HOSTLIST block, including the
		# ENDLIST line, thus causing everything to be nicely indented.
		m/^(.*);- HOSTLIST\s+(.*)/ and $rl = $2 and $lead = $1;

		# When an ENDLIST line is encountered, ouput the new block contents
		# before writing out a fresh ENDLIST line, all indented the same
		# as the HOSTLIST line.  Ignore ENDLIST's that happen before a
		# corresponding HOSTLIST line.
		if($rl and m/;- ENDLIST/)
			{
			# Write out a DNS A record entry for every host listed in
			# the dynamic entry list that matches the IP mask regex.
			for $host ( @dynahosts )
				{
				defined $dynalist{$host}{ip} and $dynalist{$host}{ip} =~ m/$rl/ and
					print OUTFILE "$lead$host$lansuffix. IN A $dynalist{$host}{ip}\n"
						. "$lead*.$host$lansuffix. IN A $dynalist{$host}{ip}\n"
				}

			# Print the ENDLIST line and reset the regex
			# and lead-in to mark us as no longer in a block
			# and to resume copying I->O.
			print OUTFILE "$lead;- ENDLIST\n";
			$lead = $rl = "";
			}
		}

	# Move the output file over top of the original.  Don't release any locks
	# until after the rewrite is done.  This should prevent any issues with a
	# user editing the file while we're trying to run, so long as they don't
	# hold the file locked TOO long...
	if(!rename("$zonea.new", "$zonea"))
		{ sleep(1); goto RESTART; }
	close(OUTFILE);
	close(INFILE);
	}
else
	{ sleep(1); goto RESTART; }

# Rewrite the reverse DNS zone with all entries that have an IP address.  This
# way, all DHCP static AND dynamic hosts, PLUS any VPN nodes will be addressable
# by symbolic hostname by any host using the DNS server here.
$lead = $rl = "";
if(open(INFILE, "< $zoneptr") and flock(INFILE, 2)
	and open(OUTFILE, "> $zoneptr.new"))
	{
	while(<INFILE>)
		{
		# If no HOSTLIST regex is defined, we're not in the middle of a 
		# HOSTLIST block, so just copy input to output.  If we're in a
		# HOSTLIST block, ignore input until ENDLIST is reached.
		$rl or print OUTFILE;

		# If a ;- HOSTLIST line is encountered, copy the IP match regex.
		# Also, copy the "lead-up" (anything before the ;-) because we'll
		# rewrite it on every line in the HOSTLIST block, including the
		# ENDLIST line, thus causing everything to be nicely indented.
		m/^(.*);- HOSTLIST\s+(.*)/ and $rl = $2 and $lead = $1;

		# When an ENDLIST line is encountered, ouput the new block contents
		# before writing out a fresh ENDLIST line, all indented the same
		# as the HOSTLIST line.  Ignore ENDLIST's that happen before a
		# corresponding HOSTLIST line.
		if($rl and m/;- ENDLIST/)
			{
			# Write out a DNS PTR record entry for every host listed in
			# the dynamic entry list that matches the IP mask regex. We
			# have to do one more regex capture here to reverse the IP
			# octets, per the zonefile standard.
			for $host ( @dynahosts )
				{
				defined $dynalist{$host}{ip} and $dynalist{$host}{ip} =~ m/$rl/
					and $dynalist{$host}{ip} =~ m/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/
					and print OUTFILE "$lead$4.$3.$2.$1.in-addr.arpa. "
                                                . "PTR $host$lansuffix.\n";
				}

			# Print the ENDLIST line and reset the regex
			# and lead-in to mark us as no longer in a block
			# and to resume copying I->O.
			print OUTFILE "$lead;- ENDLIST\n";
			$lead = $rl = "";
			}
		}

	# Move the output file over top of the original.  Don't release any locks
	# until after the rewrite is done.  This should prevent any issues with a
	# user editing the file while we're trying to run, so long as they don't
	# hold the file locked TOO long...
	if(!rename("$zoneptr.new", "$zoneptr"))
		{ sleep(1); goto RESTART; }
	close(OUTFILE);
	close(INFILE);
	}
else
	{ sleep(1); goto RESTART; }

## -----------------------------------------------------------------------------
## Restart Servers and/or Reload Configs and Zones
## -----------------------------------------------------------------------------

# Checksum the dhcpd.conf file and determine if the content is actually
# different from before it was rewritten.  If it is, DHCP server needs to be
# restarted to reload its config.
if($dhcpsum ne `cksum $dhcpdconf`)
	{
	# Run the PS command to get a list of process ID's and their
	# corresponding command lines.  Search through it for a dhcpd
	# process.  If not found, do nothing.
	@pnf = split(/\n/, `ps -axww -o pid,command`);
	if(@pnf = grep { m/dhcpd/ } @pnf)
		{
		# Capture the process ID and command line from the dhcpd
		# ps entry we found.
		"@pnf" =~ m/\s*([0-9]+)\s+(.*)/;
		$pid = $1; $cmd = $2;

		# Signal the old dhcpd PID to terminate and wait several
		# seconds to allow it to shut down gracefully.  If it doesn't
		# shut down gracefully, kill it.
		kill(15, $pid);
		$try = 0;
		while($try++ < 50 and kill(0, $pid))
			{ sleep 0.1; }
		if(kill(0, $pid))
			{ kill(9, $pid); }

		# Relaunch the daemon with the same command line options
		# it was running with before.
		system("$cmd &");
		}
	}

# If either of the zone files differ in content from before they were
# rewritten, use rndc to reload the DNS zones without restarting the
# DNS server.  rndc should be part of any BIND distro.
if($dnssum ne `cksum $zonea $zoneptr`)
	{ system("rndc reload &"); }


## -----------------------------------------------------------------------------
## Wait For Changes and Restart
## -----------------------------------------------------------------------------

# Fork into background AFTER having successfully done one update.
$forked or fork and exit;
$forked = 1;

# Open all files relevant to autohosts and watch for any kind of relevant
# file changes via kqueue.  Once any change is found, close the files and
# force a delay to better ensure files have reached a quiescent state.
$kq = IO::KQueue->new();
%kqfiles = ();
for $fn ( $hostlist, $etchosts, $dhcpdconf, $dhcpleases,
        $openvpnstatus, $zonea, $zoneptr )
	{
	open($kqfiles{$fn}, $fn) and
	$kq->EV_SET(fileno($kqfiles{$fn}), EVFILT_VNODE, EV_ADD,
		NOTE_DELETE | NOTE_RENAME | NOTE_WRITE, 0, 0);
	}
$kq->kevent();
for $fn ( values %kqfiles )
	{
	close($fn);
	}
sleep(0.1);

# NOTE: Right here, I'd add a checksum check to ensure that at least one of
# the files in question has REALLY changed content (and not just metadata,
# e.g. it was touch(1)ed.  Unfortunately, the way OpenVPN's status file works
# is really dumb and prevents this from working, because it keeps a timestamp
# in the content that's constantly changing.

# Restart the application to do another update.
goto RESTART;
