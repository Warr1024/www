$Id: README,v 1.1 2006/12/17 14:34:53 warr Exp $

1. make install, then edit /usr/local/sbin/autohosts; the first section contains config
   settings you may want to change (or at least be aware of).

2. Create a /etc/hostlist text file.  It should contain entries,
   one-per-line with 3 whitespace-separated columns: hostname, IP address, MAC address,
   for each computer on the network for which you want to statically assign.

3. Edit /etc/hosts file and add an empty comment block:
	#: HOSTLIST .
	#: ENDLIST
   where "." can be replaced with a regex on which to match dotted-quad IP addresses,
   though . (match all IP's in the system) is recommended here.

4. Edit your /etc/dhcpd.conf file and add comment blocks similar to step #3.  Note that
   your regex should reflect the subnet declaration you used.

5. Set up a forward zone and a reverse zone on your DNS server to serve the addresses
   in your network's private domain.  Put comment blocks similar to the follwing in
   each:
	;- HOSTLIST .
	;- ENDLIST
   These operate on the same principle as #3, though the comments are different because
   DNS zone files have different syntax.
