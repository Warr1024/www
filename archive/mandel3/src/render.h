#ifndef _mandel_render_
#define _mandel_render_

#include <math>
#include "BUTTON1.H"
#include "BUTTON2.H"

void mandelbrot(long double cx, long double cy, long double zm)
{

 int x, y, z;
 long double r, i, or, oi, t;

 drawbar1();
 drawbar2();

 asm {

  mov ax, 0xA000
  mov es, ax
  mov di, 10240
  mov ax, 0
  mov cx, 26880
  rep stosw
  mov bx, 10240

 }

 for(y = 32; y < 200; y++)
  for(x = 0; x < 320; x++)
  {

	or = ((x - 160) / (80 * zm)) + cx;
	oi = ((y - 116) / (42 * zm)) + cy;
	i = oi; r = or; z = 16;

	while((abs(r)< 2) && (abs(i) < 2) && (z < 254))
	{

	 t = r * r - i * i + or;
	 i = 2 * r * i + oi;
	 r = t; z++;

	}

	asm {

	 mov cx, z
	 mov es:[bx], cl
    inc bx

	}

  }

}

void palrot(double q)
{

 int x, r, g, b;

 for(x = 16; x < 255; x++)
 {

  r = 2 * sqrt(x) * (sin(.02454369260617 * (x + q)) + 1);
  g = 2 * sqrt(x) * (sin(.02454369260617 * (x + q + 85)) + 1);
  b = 2 * sqrt(x) * (sin(.02454369260617 * (x + q + 170)) + 1);

  asm {

   mov dx, 0x3C8
   mov ax, x
   out dx, al
   inc dx
   mov ax, r
   out dx, al
   mov ax, g
   out dx, al
   mov ax, b
   out dx, al

  }

 }

}

#endif
