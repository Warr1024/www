#include "RENDER.H"
#include "CAPTURE.H"
#include "INTRFACE.H"

int main()
{

 asm {

  mov ax, 0
  int 0x33
  mov ax, 0x13
  int 0x10

 }

 long double zm = 1, cx = 0, cy = 0;
 int x, y, btn, p = 0;
 palrot(0);

 rendrit:

 if(cx < -2)
  cx = -2;

 if(cx > 2)
  cx = 2;

 if(cy < -2)
  cy = -2;

 if(cy > 2)
  cy = 2;

 if(cx < -2)
  cx = -2;

 if(zm < 1)
 {

  cx = 0;
  cy = 0;
  zm = 1;

 }

 asm {

  mov ax, 2
  int 0x33

 }

 mandelbrot(cx, cy, zm);

 asm {

  mov ax, 1
  int 0x33

 }

 nextcmd:

 getmouse(x, y, btn);

 if(y >= 32)
 {

  if(btn == 1)
  {

   cx = ((x - 160) / (80 * zm)) + cx;
   cy = ((y - 116) / (42 * zm)) + cy;
   zm *= 4;
   goto rendrit;

  }

  cx = ((x - 160) / (80 * zm)) + cx;
  cy = ((y - 116) / (42 * zm)) + cy;
  zm /= 4;
  goto rendrit;

 }

 if(x < 32)
  capt(cx, cy, zm, p);

 if(x < 64)
  asm {

   mov ax, 3
   int 0x10
   mov ax, 0x4C00
   int 0x21

  }

 if(x < 96)
 {

  if(--p < 0)
   p += 256;

  palrot(p);

  goto nextcmd;

 }

 if(x < 128)
 {

  if(++p > 255)
   p -= 256;

  palrot(p);

 }

 goto nextcmd;

}
