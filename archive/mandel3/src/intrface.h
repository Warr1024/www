#ifndef _mandel_interface_
#define _mandel_interface_

void getmouse(int &x, int &y, int &b)
{

 asm {

  mov ax, 1
  int 0x33

 }

 int k = 0, ix, iy;

 do {

  asm {

	cli
	mov dx, 0x3DA
	waiton:
	in al, dx
	and al, 8
	jnz waiton
	waitoff:
	in al, dx
	and al, 8
	jz waitoff
	sti

	mov ax, 3
	int 0x33
	mov k, bx
	mov ix, cx
   mov iy, dx

  }

 } while(k == 0);

 b = k;
 x = ix / 2;
 y = iy;

}

#endif
