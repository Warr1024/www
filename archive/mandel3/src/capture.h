#ifndef _mandel_capture_
#define _mandel_capture_

#include <fstream>
#include <conio>
#include <math>
#include <io>

struct bmpheader
{

 int bfType;
 long int bfSize;
 int bfReserved1;
 int bfReserved2;
 long int bfOffBits;

 long int biSize;
 long int biWidth;
 long int biHeight;
 int biPlanes;
 int biBitCount;
 long int biCompression;
 long int biSizeImage;
 long int biXPelsPerMeter;
 long int biYPelsPerMeter;
 long int biClrUsed;
 long int biClrImportant;

};

inline void writeheader(fstream &f)
{

 bmpheader x;

 x.bfType = 19778;
 x.bfSize = 786486;
 x.bfReserved1 = 0;
 x.bfReserved2 = 0;
 x.bfOffBits = 1078;
 x.biSize = 40;
 x.biWidth = 1024;
 x.biHeight = 768;
 x.biPlanes = 1;
 x.biBitCount = 8;
 x.biCompression = 0;
 x.biSizeImage = 786432;
 x.biXPelsPerMeter = 3780;
 x.biYPelsPerMeter = 3780;
 x.biClrUsed = 0;
 x.biClrImportant = 0;

 f.write((unsigned char *) &x, sizeof(x));

}

inline void outpal(fstream &f, const double q)
{

 unsigned char r, g, b, u = 0;

 for(int x = 0; x < 256; x++)
 {

  r = (8 * sqrt(x) * (sin(.02454369260617 * (x + q)) + 1));
  g = (8 * sqrt(x) * (sin(.02454369260617 * (x + q + 85)) + 1));
  b = (8 * sqrt(x) * (sin(.02454369260617 * (x + q + 170)) + 1));

  f.put(b);
  f.put(g);
  f.put(r);
  f.put(u);

 }

}

inline void outpic(fstream &f, const long double cx, const long double cy,
                                              const long double zm)
{

 int x, y;
 unsigned char z;
 long double r, i, or, oi, t;

 for(y = 0; y < 768; y++)
 {

  for(x = 0; x < 1024; x++)
  {

	or = ((x - 512) / (256 * zm)) + cx;
	oi = (((767 - y) - 384) / (192 * zm)) + cy;
	i = oi; r = or; z = 0;

	while((abs(r) < 2) && (abs(i) < 2) && (z < 255))
	{

	 t = r * r - i * i + or;
	 i = 2 * r * i + oi;
	 r = t; z++;

	}

	f.put(z);

  }

  gotoxy(y / 10.10526315789 + 4, 7);
  cout << char(254);

 }

}


void capt(long double cx, long double cy, long double zm, const int p)
{

 fstream f;
 f.open("MNDLBROT.BMP", ios::binary | ios::out | ios::ate);
 f.seekg(0);

 asm {

  mov ax, 0
  int 0x33
  mov ax, 3
  int 0x10

 }

 if(f == 0)
 {

  cout << "ERROR WRITING TO FILE MNDLBROT.BMP\n\n";

  asm {

   mov ax, 0x4C00
   int 0x21

  }

 }

 cout << "Aaron Suen's Mandelbrot Fractal Viewer v3.00\nCapturing Fractal as a "
      << "1024x768 Windows Bitmap\n\nWriting MNDLBROT.BMP:\n  Writing Header an"
      << "d Palette . . .\n";

 writeheader(f);
 outpal(f, p);

 cout << "  Rendering and Writing Image . . .\n  [                             "
      <<"                                               ]";

 outpic(f, cx, cy, zm);
 f.close();

 cout << "\n  Done!\n\n";

 asm {

  mov ax, 0x4C00
  int 0x21

 }

}

#endif
