using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Remote Desktop via HTTP Proxy")]
[assembly: AssemblyDescription("Allows RDP connections via an arbitrary HTTP proxy")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Remote Desktop via HTTP Proxy")]
[assembly: AssemblyCopyright("(C)2006 Aaron Suen")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

[assembly: AssemblyVersion("0.9.*")]

[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]