using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Drawing;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Security.Cryptography;
using Microsoft.Win32;

namespace ProxyRDP
{
	public class FormConnect : System.Windows.Forms.Form
	{
		#region Fields
		protected System.Net.Sockets.Socket ProxySocket = null;
		protected System.Net.Sockets.Socket ListenerSocket = null;
		protected System.Net.Sockets.Socket ClientSocket = null;
		protected System.Windows.Forms.TextBox tbProxServ;
		protected System.Windows.Forms.Label lblProxServ;
		protected System.Windows.Forms.Label lblProxPort;
		protected System.Windows.Forms.Label lblRDPServ;
		protected System.Windows.Forms.TextBox tbProxPort;
		protected System.Windows.Forms.Button btnConnect;
		protected System.Windows.Forms.PictureBox picBanner;
		protected System.Windows.Forms.Button btnCancel;
		protected System.Windows.Forms.ComboBox tbRDPServ;
		protected System.Windows.Forms.LinkLabel lnkLicense;
		private System.ComponentModel.Container components = null;
		public string[] Args = null;
		#endregion

		#region Entry Point
		[STAThread]
		static void Main(string[] Argv)
		{
			FormConnect Conn = new FormConnect();
			Conn.Args = Argv;
			Application.Run(Conn);
		}
		#endregion

		#region Construct/Dispose
		public FormConnect()
		{
			InitializeComponent();
		}
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FormConnect));
			this.tbProxServ = new System.Windows.Forms.TextBox();
			this.lblProxServ = new System.Windows.Forms.Label();
			this.lblProxPort = new System.Windows.Forms.Label();
			this.lblRDPServ = new System.Windows.Forms.Label();
			this.tbProxPort = new System.Windows.Forms.TextBox();
			this.btnConnect = new System.Windows.Forms.Button();
			this.picBanner = new System.Windows.Forms.PictureBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.tbRDPServ = new System.Windows.Forms.ComboBox();
			this.lnkLicense = new System.Windows.Forms.LinkLabel();
			this.SuspendLayout();
			// 
			// tbProxServ
			// 
			this.tbProxServ.Location = new System.Drawing.Point(168, 104);
			this.tbProxServ.Name = "tbProxServ";
			this.tbProxServ.Size = new System.Drawing.Size(120, 21);
			this.tbProxServ.TabIndex = 3;
			this.tbProxServ.Text = "";
			// 
			// lblProxServ
			// 
			this.lblProxServ.Location = new System.Drawing.Point(8, 104);
			this.lblProxServ.Name = "lblProxServ";
			this.lblProxServ.Size = new System.Drawing.Size(152, 16);
			this.lblProxServ.TabIndex = 2;
			this.lblProxServ.Text = "HTTP Proxy:";
			this.lblProxServ.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblProxPort
			// 
			this.lblProxPort.Location = new System.Drawing.Point(304, 104);
			this.lblProxPort.Name = "lblProxPort";
			this.lblProxPort.Size = new System.Drawing.Size(32, 16);
			this.lblProxPort.TabIndex = 4;
			this.lblProxPort.Text = "Port:";
			this.lblProxPort.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblRDPServ
			// 
			this.lblRDPServ.Location = new System.Drawing.Point(8, 80);
			this.lblRDPServ.Name = "lblRDPServ";
			this.lblRDPServ.Size = new System.Drawing.Size(152, 16);
			this.lblRDPServ.TabIndex = 0;
			this.lblRDPServ.Text = "Remote Desktop Computer:";
			this.lblRDPServ.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbProxPort
			// 
			this.tbProxPort.Location = new System.Drawing.Point(344, 104);
			this.tbProxPort.Name = "tbProxPort";
			this.tbProxPort.Size = new System.Drawing.Size(56, 21);
			this.tbProxPort.TabIndex = 5;
			this.tbProxPort.Text = "";
			// 
			// btnConnect
			// 
			this.btnConnect.Location = new System.Drawing.Point(200, 136);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(104, 24);
			this.btnConnect.TabIndex = 6;
			this.btnConnect.Text = "Connect";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// picBanner
			// 
			this.picBanner.Image = ((System.Drawing.Image)(resources.GetObject("picBanner.Image")));
			this.picBanner.Location = new System.Drawing.Point(0, 0);
			this.picBanner.Name = "picBanner";
			this.picBanner.Size = new System.Drawing.Size(405, 70);
			this.picBanner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picBanner.TabIndex = 7;
			this.picBanner.TabStop = false;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(312, 136);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(88, 24);
			this.btnCancel.TabIndex = 7;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// tbRDPServ
			// 
			this.tbRDPServ.Location = new System.Drawing.Point(168, 80);
			this.tbRDPServ.Name = "tbRDPServ";
			this.tbRDPServ.Size = new System.Drawing.Size(232, 21);
			this.tbRDPServ.TabIndex = 1;
			// 
			// lnkLicense
			// 
			this.lnkLicense.AutoSize = true;
			this.lnkLicense.Location = new System.Drawing.Point(8, 144);
			this.lnkLicense.Name = "lnkLicense";
			this.lnkLicense.Size = new System.Drawing.Size(101, 17);
			this.lnkLicense.TabIndex = 8;
			this.lnkLicense.TabStop = true;
			this.lnkLicense.Text = "�2006 Aaron Suen";
			this.lnkLicense.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLicense_LinkClicked);
			// 
			// FormConnect
			// 
			this.AcceptButton = this.btnConnect;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(408, 165);
			this.Controls.Add(this.lnkLicense);
			this.Controls.Add(this.tbRDPServ);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.picBanner);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.tbProxPort);
			this.Controls.Add(this.tbProxServ);
			this.Controls.Add(this.lblRDPServ);
			this.Controls.Add(this.lblProxPort);
			this.Controls.Add(this.lblProxServ);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FormConnect";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Closing += new System.ComponentModel.CancelEventHandler(this.FormConnect_Closing);
			this.Load += new System.EventHandler(this.FormConnect_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region Form Load
		private void FormConnect_Load(object sender, System.EventArgs e)
		{
			// Display product and version info in main window title.
			this.Text = Application.ProductName + " v" + Application.ProductVersion;

			// Load the Remote Desktop MRU list from the registry, and ignore
			// any entries generated by ProxyRDP (begin with 127.0.0.1:).  Add
			// them to the dropdown combo.
			RegistryKey K = Registry.CurrentUser.OpenSubKey
				(@"Software\Microsoft\Terminal Server Client\Default");
			if(K != null)
				for(int KN = 0; KN < 10; KN++)
				{
					object O = K.GetValue("MRU" + KN.ToString());
					if(O == null)
						continue;
					if(O.ToString().StartsWith(IPAddress.Loopback.ToString() + ":"))
						continue;
					this.tbRDPServ.Items.Add(O.ToString());
				}
			if(this.tbRDPServ.Items.Count > 0)
				this.tbRDPServ.SelectedIndex = 0;

			// Try to open the Windows Internet Settings registry key to find
			// the current default HTTP proxy settings.  If we can't find this
			// key and corresponding value, do nothing (user can manually enter
			// proxy settings).
			K = Registry.CurrentUser.OpenSubKey
				(@"Software\Microsoft\Windows\CurrentVersion\Internet Settings");
			if(K != null)
			{
				object O = K.GetValue("ProxyServer");
				if(O != null)
				{
					// Split the proxy server list by semicolons and look for either an
					// all-protocols proxy, or an HTTP proxy.
					string[] Servs = O.ToString().Split(';');
					foreach(string S in Servs)
						if((S.ToUpper().IndexOf("HTTP=") >= 0) || (S.IndexOf("=") < 0))
						{
							// Strip off any protocol= prefix, leaving just the
							// server:port value.  There MUST be both a server
							// and port number for the value to be useable.
							string Z = S;
							if(Z.IndexOf("=") >= 0)
								Z = Z.Substring(Z.IndexOf("=") + 1);
							if(Z.IndexOf(":") < 0)
								continue;

							// Initialize the server name and port textboxes.
							this.tbProxServ.Text = Z.Split(':')[0];
							this.tbProxPort.Text = Z.Split(':')[1];

							// Skip the rest of the proxy specs (we've already
							// got what we want here).
							break;
						}
				}
			}

			// For some reason, the form will often not show up in the taskbar
			// because of its FormBorderStyle.  Toggle this variable after load
			// to force it into the taskbar.
			this.ShowInTaskbar = false;
			this.ShowInTaskbar = true;
			this.Activate();

			// Currently, we can accept a single host on the commandline to use
			// as the remote host to look up via the default IE HTTP proxy.
			// Eventually, I'd like to accept proxy host, port, and even proxy
			// type (other than HTTP) on the command line...
			if((Args != null) && (Args.Length > 0))
			{
				this.tbRDPServ.Text = Args[0];
				this.Refresh();
				this.btnConnect_Click(null, null);
			}
		}
		#endregion

		#region Cancel Button
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
		#endregion

		#region Connect Button
		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			// Validate some user input values first.
			UInt16 ProxyPort = 0;
			try { ProxyPort = Convert.ToUInt16(this.tbProxPort.Text); }
			catch { }
			if(ProxyPort < 1)
			{
				MessageBox.Show("Proxy server port must be an integer between "
					+ "0 and 65535.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			IPAddress ProxyAddr = null;
			try { ProxyAddr = Dns.GetHostByName(this.tbProxServ.Text).AddressList[0]; }
			catch { }
			if(ProxyAddr == null)
			{
				MessageBox.Show("Unable to locate proxy server \"" + this.tbProxServ.Text
					+ "\".", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			// Disable all UI controls, as the user should no longer be allowed
			// to change these settings.  The app is single-use only.
			foreach(Control C in this.Controls)
				C.Enabled = false;
			this.Refresh();

			// Catch any exceptions that may happen during this process.  If such
			// an exception does happen, notify the user with a popup message and
			// shut down the application.
			try
			{
				// Connect to the HTTP proxy.
				ProxySocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream,
					ProtocolType.Tcp);
				ProxySocket.Blocking = true;
				ProxySocket.Connect(new IPEndPoint(ProxyAddr, ProxyPort));

				// Send a CONNECT request.  This is used to access HTTPS and other services
				// (other than strict HTTP) through such a proxy.  Nearly any kind of TCP
				// service is accessible this way.
				byte[] ToSend = Encoding.UTF8.GetBytes("CONNECT " + this.tbRDPServ.Text
					+ ":3389 HTTP/1.0\r\n\r\n");
				ProxySocket.Send(ToSend);

				// Poll the connection until a valid HTTP response header is received.
				DateTime PollStart = DateTime.Now;
				string S = string.Empty;
				while(S.Replace("\n", "").IndexOf("\r\r") < 0)
				{
					if(DateTime.Now > PollStart.AddSeconds(20))
						throw new Exception("Proxy server did not send a recognizable "
							+ "response within the timeout period.");
					if(ProxySocket.Available > 0)
					{
						byte[] B = new byte[ProxySocket.Available];
						int R = ProxySocket.Receive(B);
						S += Encoding.UTF8.GetString(B, 0, R);
					}
					else
						Thread.Sleep(100);
				}

				// Skip the HTTP protocol version (irrelevant here) and make sure that
				// the response code is 200 (which means it connected to the requested
				// address).  If response is not 200, throw an exception.
				if(S.Substring(S.IndexOf(" ") + 1, 3) != "200")
				{
					S = S.Replace("\r", "");
					S = S.Substring(0, S.IndexOf("\n\n"));
					S = S.Replace("\n", "\r\n");
					throw new Exception("Failed to connect to " + this.tbRDPServ.Text
						+ Environment.NewLine + Environment.NewLine + "Proxy Server Response:"
						+ Environment.NewLine + S);
				}

				// Iteratively try random local ports in the private port area
				// (49152-65535) until we find an open one that we can bind to,
				// and begin listening on that socket.
				int Port = 0;
				while(ListenerSocket == null)
				{
					// Generate random numbers and compute a good port out of them.
					byte[] PortBytes = new byte[2];
					new RNGCryptoServiceProvider().GetBytes(PortBytes);
					Port = (int)PortBytes[0] | ((int)(PortBytes[1] & 63) << 8) + 49152;

					// Try to listen on the chosen port.  If it fails, reset the listener
					// socket to try again.
					try
					{
						ListenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream,
							ProtocolType.Tcp);
						ListenerSocket.Bind(new IPEndPoint(IPAddress.Loopback, Port));
						ListenerSocket.Listen(1);
					}
					catch { ListenerSocket = null; }
				}

				// Launch the REAL Terminal Services Client, and pass in OUR local connection
				// (which will pass through the already-connected proxy) on the command line.
				Process.Start(@"C:\windows\system32\mstsc.exe",
					"/v:" + IPAddress.Loopback.ToString() + ":" + Port.ToString());

				// Wait a few seconds for the RDP client to connect to the proxy socket.
				ListenerSocket.Blocking = false;
				DateTime AcceptStart = DateTime.Now;
				while(ClientSocket == null)
				{
					if(DateTime.Now > AcceptStart.AddSeconds(5))
						throw new Exception("RDP client did not connect to the ProxyRDP listening "
							+ "endpoint in a timely fashion.");
					try { ClientSocket = ListenerSocket.Accept(); }
					catch { }
					Thread.Sleep(100);
				}
				ClientSocket.Blocking = true;
			}
			catch(Exception Ex)
			{
				// Display exception message as a simple popup.
				MessageBox.Show(Ex.Message, this.Text, MessageBoxButtons.OK,
					MessageBoxIcon.Stop);

				// Shut down the app on any error.
				this.Close();
			}

			// Don't display any exceptions past this point.
			try
			{
				// Hide the application main window (since it won't be responsive now
				// anyway, and we need to allow a little time to shut it down after either
				// side closes a connection.
				this.Visible = false;

				// Now, enter the main loop, passing data from socket to socket until
				// one or the other closes.
				while(ClientSocket.Connected && ProxySocket.Connected)
				{
					// Watch both direction sockets for any available data
					// or any error conditions for up to one second.  This
					// should cause the current thread to sleep, preventing
					// CPU pegging.
					ArrayList RdSox = new ArrayList();
					RdSox.Add(ClientSocket);
					RdSox.Add(ProxySocket);
					ArrayList ErSox = new ArrayList();
					ErSox.Add(ClientSocket);
					ErSox.Add(ProxySocket);
					Socket.Select(RdSox, null, ErSox, 5000000);

					// If any sockets are in an error state, abort the loop.
					if((ErSox.Count > 0)
						|| (ClientSocket == null) || !ClientSocket.Connected
						|| (ProxySocket == null) || !ProxySocket.Connected)
						break;

					// If either socket has data available, read it in and
					// shuttle it across to the other socket.  Do immediate
					// (no Nagle) sends, since the RDP protocol is assumed
					// to have segmented the traffic as it likes.
					if(ClientSocket.Available > 0)
					{
						byte[] B = new byte[ClientSocket.Available];
						int L = ClientSocket.Receive(B);
						ProxySocket.Send(B, L, SocketFlags.Partial);
					}
					else if(ProxySocket.Available > 0)
					{
						byte[] B = new byte[ProxySocket.Available];
						int L = ProxySocket.Receive(B);
						ClientSocket.Send(B, L, SocketFlags.Partial);
					}
					else
					{
						// For some reason, one endpoint or the other seems to
						// be able to shut down and leave my sockets in a
						// still-connected state, which leaves the process running
						// long after it should have shut down.  Send a zero-sized
						// TCP "keepalive" packet in either direction to try to
						// detect this condition and provoke any socket shutdown
						// that needs to happen.  Also prevents proxies/firewalls
						// from unduly closing an idle yet still valid connection.
						ClientSocket.Send(new byte[0], SocketFlags.Partial);
						ProxySocket.Send(new byte[0], SocketFlags.Partial);
						Thread.Sleep(10);
					}
				}
			}
			catch
			{
				// On error, just assume that the connections were properly closed,
				// and disconnect.  No need to display any error messages.
				this.Close();
			}
		}
		#endregion

		#region Form Closing
		private void FormConnect_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If there are any leftover running
			// sockets, close all of them.  This should
			// also force any of the background threads
			// for these sockets to shut down too.
			try
			{
				if(ListenerSocket != null)
					ListenerSocket.Close();
			}
			catch { }
			try
			{
				if(ClientSocket != null)
					ClientSocket.Close();
			}
			catch { }
			try
			{
				if(ProxySocket != null)
					ProxySocket.Close();
			}
			catch { }
		}
		#endregion

		#region License LinkButton
		private void lnkLicense_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			StringBuilder SB = new StringBuilder();
			SB.Append("Copyright (C) 2006, Aaron Suen" + Environment.NewLine);
			SB.Append("All rights reserved." + Environment.NewLine);
			SB.Append(Environment.NewLine);
			SB.Append("Redistribution and use in source and binary forms, with or without" + Environment.NewLine);
			SB.Append("modification, are permitted provided that the following conditions are" + Environment.NewLine);
			SB.Append("met:" + Environment.NewLine);
			SB.Append(Environment.NewLine);
			SB.Append("    * Redistributions of source code must retain the above copyright" + Environment.NewLine);
			SB.Append("      notice, this list of conditions and the following disclaimer." + Environment.NewLine);
			SB.Append("    * Redistributions in binary form must reproduce the above copyright" + Environment.NewLine);
			SB.Append("      notice, this list of conditions and the following disclaimer in" + Environment.NewLine);
			SB.Append("      the documentation and/or other materials provided with the" + Environment.NewLine);
			SB.Append("      distribution." + Environment.NewLine);
			SB.Append("    * Neither the name of the copyright holder(s) nor the names of any" + Environment.NewLine);
			SB.Append("      contributors to this software may be used to endorse or promote" + Environment.NewLine);
			SB.Append("      products derived from this software without specific prior written" + Environment.NewLine);
			SB.Append("      permission." + Environment.NewLine);
			SB.Append(Environment.NewLine);
			SB.Append("THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER(S) AND CONTRIBUTOR(S)" + Environment.NewLine);
			SB.Append("\"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT" + Environment.NewLine);
			SB.Append("LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR" + Environment.NewLine);
			SB.Append("A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT" + Environment.NewLine);
			SB.Append("OWNER(S) OR CONTRIBUTOR(S) BE LIABLE FOR ANY DIRECT, INDIRECT," + Environment.NewLine);
			SB.Append("INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING," + Environment.NewLine);
			SB.Append("BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS" + Environment.NewLine);
			SB.Append("OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND" + Environment.NewLine);
			SB.Append("ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR" + Environment.NewLine);
			SB.Append("TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE" + Environment.NewLine);
			SB.Append("USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE." + Environment.NewLine);
			MessageBox.Show(SB.ToString(), Application.ProductName + " License", MessageBoxButtons.OK,
				MessageBoxIcon.Information);
		}
		#endregion
	}
}
